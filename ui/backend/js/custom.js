function getSubcategory()
{
	let cat_id=$("#category_id").val();

	$.get('/findsubcategory/'+cat_id, function (data)
	{
		let sub_id = $('#sub_id');
		sub_id.empty();
		sub_id.append('<option value="0" disabled="true" selected="true">--select sub category--</option>');

		for(let subCategory of JSON.parse(data))
		{
			sub_id.append('<option value="'+ subCategory.id +'">'+subCategory.title+'</option>');

		}
	});

}


