<?php


class Category extends MX_Controller
{
	public function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Category_model');
		$this->load->library('session');
		if(!$_SESSION['user_email'])
		{
			redirect('login','refresh');
		}

	}

	public function categoryCreate()
	{

		$this->load->view('category/create.php');
	}

	public function categoryList()
	{
		$category['categories']=$this->Category_model->getAllCategories();

		$this->load->view('category/list.php',$category);
	}

	public function categoryStore()
	{

		$category['name'] = $this->input->post('name');

		$category['created_at'] =date('Y-m-d h:i:a');
		$category['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->Category_model->categoryInsert($category);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Category Add Successfully.</span>');

		redirect('category/list','refresh');

	}

	public function categoryEdit()
	{
		$id = $this->uri->segment(3);

		$category['category'] = $this->Category_model->getCategory($id);


		$this->load->view('category/edit',$category);
	}

	public function categoryUpdate()
	{

		$id = $this->uri->segment(3);

		$categoryData= $this->Category_model->getCategory($id);
		$category['name'] = $this->input->post('name');
		$category['created_at'] = $categoryData['created_at'];
		$category['updated_at'] =date('Y-m-d h:i:a');
		$query = $this->Category_model->categoryUpdate($category,$id);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Category Update Successfully.</span>');

		redirect('category/list','refresh');

	}

	public function delete(){
		$id = $this->uri->segment(3);

		$query = $this->Category_model->deleteCategory($id);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Category Delete Successfully.</span>');

		redirect('category/list','refresh');
	}

}
