<?php


class Category_model extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function categoryInsert($category){


		$this->db->insert('categories', $category);

	}
	public function getAllCategories()
	{
		$query = $this->db->get('categories');
		return $query->result();
	}

	public function getCategory($id)
	{
		$query = $this->db->get_where('categories',array('id'=>$id));
		return $query->row_array();
	}

	public function categoryUpdate($category, $id)
	{
		$this->db->where('categories.id', $id);
		return $this->db->update('categories', $category);
	}

	public function deleteCategory($id)
	{
		$this->db->where('categories.id', $id);
		return $this->db->delete('categories');
	}


}
