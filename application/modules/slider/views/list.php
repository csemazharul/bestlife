<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<?php if(isset($_SESSION['success']))
				{
					?>
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<div class="alert-icon">
							<i class="icon-check"></i>
						</div>
						<div class="alert-message">
							<?php echo $this->session->flashdata('success'); ?>
						</div>
					</div>
					<?php
				}
				?>
				<div class="card">
					<div class="card-header"><i class="fa fa-table"></i> Page List</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="default-datatable" class="table table-bordered">
								<thead>
								<tr>
									<th>Id</th>
									<th>Title</th>
									<th>Short Description</th>
									<th>Picture</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								<?php
								foreach($sliders as $slider){
									?>
									<tr>
										<td><?php echo $slider->id; ?></td>
										<td><?php echo $slider->title; ?></td>
										<td><?php echo $slider->short_description; ?></td>
										<td><img src="<?php echo base_url(); ?>upload/images/<?php echo $slider->picture; ?>" width="100px" height="80px"/></td>
										<td><a href="<?php echo base_url(); ?>slider/edit/<?php echo $slider->id; ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="<?php echo base_url(); ?>slider/delete/<?php echo $slider->id; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</a></td>
									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
