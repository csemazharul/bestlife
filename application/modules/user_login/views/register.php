<?php
$this->load->view('frontend/layout/header');
?>
<div class="breadcrumb_wrapper">

		<div class="breadcrumb_block">
				<ul>
						<li><a href="index.html">home</a></li>
						<li>Registration</li>
				</ul>
		</div>
</div>

<!--Contact Form-->
<div class=" contact_form_wrapper clv_section">
		<div class="container">
				<div class="row">
						<div class="col-lg-8 col-md-8 offset-lg-2">
								<div class="contact_form_section" style="box-shadow:0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);border:none;">
										<div class="row">

												<div class="col-md-12 col-lg-12">
														<h3>Registration</h3>
														<div id="the-message"></div>

												</div>



					<form id="signupForm" action="<?php echo base_url() ?>user/registration/store" method="post">

											<div class="col-md-6 col-lg-6">
													<div class="form_block">
														<label for="country_code">Select Country</label>

															<select class="form-control" id="country_code" name="country_code">
																<option value="0">choose Country</option>
																 <option value="USA">USA</option>
																 <option value="BAN">BAN</option>
															</select>

													</div>
											</div>
											<div class="col-md-6 col-lg-6">
													<div class="form_block">
															<label for="sponser_id">Sponcor ID</label>
															<input type="text" id="sponser_id" name="sponser_id" class="form-control" placeholder="Sponsor ID" >

													</div>
											</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="firstname">First Name</label>
																<input type="text" id="firstname" name="firstname" class="form-control" placeholder="First Name" >

														</div>
												</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="lastname">Last Name</label>
																<input type="text" id="lastname" name="lastname" class="form-control" placeholder="Last Name" >

														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="date_of_birth">Date Of Birth</label>
																<input class="form-control" id="date_of_birth" type="date" name="date_of_birth">

														</div>
												</div>


												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="corrency">select corrency</label>
																<select id="currency" name="currency" class='form-control'>
																	<option value="0">choose currency</option>
																	 <option value="USD">USD</option>
																	 <option value="EURO">EURO</option>
																	 <option value="BDT">BDT</option>
																</select>


														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="fullname">Full Name</label>
																<input type="text" id="fullname" name="fullname" class="form-control" placeholder="Full Name" >

														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="username">Username</label>
																<input type="text" id="username" name="username" class="form-control" placeholder="Username" >

														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<label for="user_id">User ID</label>
																<input type="text" id="user_id" name="user_id" class="form-control" placeholder="user id" >

														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="password">Password</label>
																<input type="password" id="password" name="password"  class="form-control" placeholder="Password" >

														</div>
												</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="c_password">Re-Password</label>
																<input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="Re-password" >

														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="t_pin">T-Pin</label>
																<input type="text" id="t_pin" name="t_pin"  class="form-control" placeholder="T-Pin">

														</div>
												</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label for="tre_pin">TRe-Pin</label>
																<input type="text" id="tre_pin" name="tre_pin" class="form-control" placeholder="TRe-Pin" >

														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															<label id="contact_no">Contact No</label>
																<input type="text" id="mobile_no" name="mobile_no" class="form-control" data-valid="number" placeholder="Contact No">

														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
															  <label for="email">Email</label>
																<input type="text" id="email" name="email"  class="form-control" placeholder="Email" >

														</div>
												</div>

												<div class="col-md-12 col-lg-12">
														<div class="form_block">
																<button type="submit" class="clv_btn">Submit</button>
														</div>
												</div>
					</form>
										</div>
								</div>
						</div>

				</div>
		</div>
</div>

<script type='text/javascript'>
// var btn = document.getElementById("button");
// btn.addEventListener("click",function(e){
//     let email=document.getElementById("email").value;
// 		if(email=='')
// 		{
// 			console.log("fillup your email");
// 			e.preventDefault();
// 		}
//
// },false);



</script>
<?php
$this->load->view('frontend/layout/footer');
?>

<script>


$('#signupForm').submit(function(e) {
	e.preventDefault();


	var me = $(this);

	// perform ajax
	$.ajax({
		url: me.attr('action'),
		type: 'post',
		data: me.serialize(),
		dataType: 'json',
		success: function(response) {

			if (response.success == true) {
				let title="Register Successfully";
							swal({
					  title: title,
			  		text: "Please check your email account",
			  		icon: "success",
								});


				// if success we would show message
				// and also remove the error class
				// $('#the-message').append('<div class="alert alert-success">' +
				// 	'<span class="glyphicon glyphicon-ok"></span>' +
				// 	'<strong>Thank you!</strong>Your Mail has been sent successfully' +
				// 	'</div>');
				$('.form_block').removeClass('has-error')
								.removeClass('has-success');
				$('.text-danger').remove();

				// reset the form
				me[0].reset();

				// close the message after seconds
				$('.alert-success').delay(500).show(10, function() {
					$(this).delay(3000).hide(10, function() {
						$(this).remove();
					});
				})
			}
			else {

				$.each(response.messages, function(key, value) {

					var element = $('#' + key);

					element.closest('div.form_block')
					.removeClass('has-error')
					.addClass(value.length > 0 ? 'has-error' : 'has-success')
					.find('.text-danger')
					.remove();


					element.after(value);
				});
			}
		}
	});
});
</script>
<!-- <script>
$('#button').click(function(e)
			{
					let country_code=$('#country_code').val();
					let currency=$('#currency').val();
					if(country_code=='')
					{
						    error.addClass( "help-block").css('color','#D93025');
							e.preventDefault();
					}
					if(currency=='')
					{
						error.addClass( "help-block").css('color','#D93025');
					 e.preventDefault();

					}

			});

</script> -->
