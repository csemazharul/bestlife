<?php
$this->load->view('/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><i class="fa fa-table"></i> sponcor  history</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="default-datatable" class="table table-bordered">
								<thead>
								<tr>
									<th>Id</th>
									<th>sponser id</th>
									<th>firstname</th>
									<th>lastname</th>
									<th>t pin</th>
									<th>tre pin</th>
                  <th>c balance</th>
									<th>currency</th>
				

								</tr>
								</thead>
								<tbody>
								<?php
								foreach($users as $user){
									?>
									<tr>
										<td><?php echo $user->id; ?></td>
										<td><?php echo $user->sponser_id; ?></td>
										<td><?php echo $user->firstname; ?></td>
										<td><?php echo $user->lastname; ?></td>
										<td><?php echo $user->t_pin; ?></td>
										<td><?php echo $user->tre_pin; ?></td>
										<td><?php echo $user->c_balance; ?></td>
										<td><?php echo $user->currency; ?></td>




									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('/layout/footer');
?>
