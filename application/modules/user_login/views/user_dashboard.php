<?php
$this->load->view('/layout/header');
?>
<div class="content-wrapper">
  <div class="container-fluid">

    <!--Start Dashboard Content-->

    <div class="card-group">
      <div class="card">


        <div class="card-body">
            <h3 class="text-center">MY ACCOUNT OVERVIEW</h3>
            <p class="text-center">BALANCE : <?php echo $balance?> BDT  </p>
        </div>

      </div>
    </div>

    <div class="row mt-4">
      <div class="col-12 col-lg-6 col-xl-4">
        <div class="card gradient-purpink">
          <div class="card-body">
            <div class="media">
              <div class="media-body text-left">
                <h4 class="text-white text-center">MY PROFILE</h4>
                <span class="text-white"></span>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-6 col-xl-4">
        <div class="card gradient-scooter">
          <div class="card-body">
            <div class="media">
              <div class="media-body text-left">
                <h4 class="text-white text-center">MY NETWORK</h4>
                <span class="text-white"></span>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-6 col-xl-4">
        <div class="card gradient-ibiza">
          <div class="card-body">
            <div class="media">
              <div class="media-body text-left">
                <h4 class="text-white text-center">BOARD</h4>
                <span class="text-white"></span>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mt-4">
      <div class="col-12 col-lg-6 col-xl-4">
        <div class="card gradient-purpink">
          <div class="card-body">
            <div class="media">
              <div class="media-body text-left">
                <h4 class="text-white text-center">MY FINANCE</h4>
                <span class="text-white"></span>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-6 col-xl-4">
        <div class="card gradient-scooter">
          <div class="card-body">
            <div class="media">
              <div class="media-body text-left">
                <h4 class="text-white text-center">SUPPORT</h4>
                <span class="text-white"></span>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-6 col-xl-4">
        <div class="card gradient-ibiza">
          <div class="card-body">
            <div class="media">
              <div class="media-body text-left">
                <h4 class="text-white text-center">NEWSLATER</h4>
                <span class="text-white"></span>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>



    <!--End Dashboard Content-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper
<?php
$this->load->view('/layout/footer');
?>
