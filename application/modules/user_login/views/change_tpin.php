<?php
$this->load->view('/layout/header');
?>
<div class="content-wrapper">
  <div class="container-fluid">

    <!--Start Dashboard Content-->

    <div class="card-group">
      <div class="card">

        <div class="card-header">
            <h5 class="text-center">CHANGE ACCOUNT T-PIN</h5>
        </div>

        <div class="card-body">
          <?php if(isset($_SESSION['success']))
          {
          ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <div class="alert-icon">
                <i class="icon-check"></i>
              </div>
              <div class="alert-message">
                <?php echo $this->session->flashdata('success'); ?>
              </div>
            </div>
          <?php
          }
          ?>

          <?php if(isset($_SESSION['errorMessage']))
          {
            ?>


            <div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <div class="alert-icon">
                <i class="icon-exclamation"></i>
              </div>
              <div class="alert-message">
                <?php echo $this->session->flashdata('errorMessage'); ?>
              </div>
            </div>
            <?php
          }
          ?>

          <form method="post" action="<?php echo base_url()?>change_tpin/update" enctype="multipart/form-data">

            <div class="form-group row">
              <label for="basic-input" class="col-sm-3 col-form-label">Old T-Pin :</label>
              <div class="col-sm-9">
                <input type="text" value="<?php echo set_value('old_tpin')?>"  name="old_tpin" id="basic-input" class="form-control">
                <span id="input-14-error" class="error"><?php echo form_error('old_tpin'); ?></span>
              </div>
            </div>

              <div class="form-group row">
              <label for="basic-input" class="col-sm-3 col-form-label">New T-Pin :</label>
              <div class="col-sm-9">
                <input type="text" value="<?php echo set_value('t_pin')?>" name="t_pin" id="basic-input" class="form-control">
                <span id="input-14-error" class="error"><?php echo form_error('t_pin'); ?></span>
              </div>
              </div>

              <div class="form-group row">
              <label for="basic-input" class="col-sm-3 col-form-label">Confirm T-Pin :</label>
              <div class="col-sm-9">
                <input type="text" value="<?php echo set_value('c_tpin')?>" name="c_tpin" id="basic-input" class="form-control">
                <span id="input-14-error" class="error"><?php echo form_error('c_tpin'); ?></span>
              </div>
              </div>

              <div class="form-group row">
              <label for="basic-input" class="col-sm-3 col-form-label">Account Password :</label>
              <div class="col-sm-9">
                <input type="password" value="<?php echo set_value('password')?>" name="password" id="basic-input" class="form-control">
                <span id="input-14-error" class="error"><?php echo form_error('password'); ?></span>
              </div>
              </div>

            <div class="form-footer row">
              <div class="col-sm-10">
                <p> Don't share your T-Pin with others.</p>
              </div>
              <div class="col-sm-2">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Update</button>
              </div>


            </div>

          </form>
        </div>

        </div>

      </div>
    </div>


    <!--End Dashboard Content-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper
<?php
$this->load->view('/layout/footer');
?>
