<?php
$this->load->view('/layout/header');
?>
<div class="content-wrapper">
  <div class="container-fluid">

    <!--Start Dashboard Content-->

    <div class="card-group">
      <div class="card">

        <div class="card-header">
            <h5 class="text-center">Your Account T-Pin is :   <?php echo $tpin->t_pin?></h5>
        </div>

        <div class="card-footer">
          <a  href="<?php echo base_url('user/dashboard')?>" class="btn btn-primary" >Back</a>
        </div>

        </div>

        </div>

      </div>
    </div>


    <!--End Dashboard Content-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper
<?php
$this->load->view('/layout/footer');
?>
