
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/dashrock/color-admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Feb 2019 03:52:33 GMT -->
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<title>Best Life Ltd.</title>
	<!--favicon-->
	<link rel="icon" href="<?php echo base_url('ui/frontend/images/')?>/logo11.png" type="image/x-icon">
	<!-- notifications css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>ui/backend/plugins/notifications/css/lobibox.min.css"/>
	<!-- Vector CSS -->
	<link href="<?php echo base_url(); ?>ui/backend/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"/>
	<!-- simplebar CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
	<!-- Bootstrap core CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/bootstrap.min.css" rel="stylesheet"/>
	<!-- animate CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/animate.css" rel="stylesheet" type="text/css"/>
	<!-- Icons CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/icons.css" rel="stylesheet" type="text/css"/>
	<!-- Sidebar CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/sidebar-menu.css" rel="stylesheet"/>
	<!-- Custom Style-->
	<link href="<?php echo base_url(); ?>ui/backend/css/app-style.css" rel="stylesheet"/>

</head>

<body onload="info_noti()">

<!-- Start wrapper-->
<div id="wrapper">

	<!--Start sidebar-wrapper-->
	<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
		<div class="brand-logo">
			<a href="index.html">
				<img src="<?php echo base_url('ui/frontend/images/')?>/logo11.png" class="logo-icon" alt="logo icon">
				<h5 class="logo-text"> Best Life Ltd</h5>
			</a>
		</div>
		<div class="user-details">
			<div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
				<div class="avatar"><img class="mr-3 side-user-img" src="<?php echo base_url(); ?>ui/backend/images/avatars/avatar-4.png" alt="user avatar"></div>
				<div class="media-body">
					<h6 class="side-user-name"><?php if(isset($_SESSION['firstname'])) echo $_SESSION['firstname']; ?></h6>
				</div>
			</div>
			<div id="user-dropdown" class="collapse">
				<ul class="user-setting-menu">
					<li><a href="javaScript:void();"><i class="icon-user"></i>  My Profile</a></li>
					<li><a href="javaScript:void();"><i class="icon-settings"></i> Setting</a></li>
					<li>


						<?php
							if($this->session->has_userdata('user_email'))
							{
						?>
						<a href="<?php echo base_url()?>admin_login/user/user_logout""><i class="icon-power"></i> Logout</a>
						<?php
						}
						if($this->session->has_userdata('email'))
						{
						?>
							<a href="<?php echo base_url()?>user_login/user/user_logout""><i class="icon-power"></i> Logout</a>
						<?php
						}
						?>

					</li>

				</ul>
			</div>
		</div>
		<ul class="sidebar-menu do-nicescrol">
			<li class="sidebar-header">MAIN NAVIGATION</li>
			<li>
				<a href="index.html" class="waves-effect">
					<i class="icon-home"></i><span>Dashboard</span><i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="sidebar-submenu">
					<li><a href="<?php echo base_url('user/dashboard'); ?>"><i class="fa fa-long-arrow-right"></i> Dashboard</a></li>
				</ul>
			</li>

			<li>
				<a href="javaScript:void();" class="waves-effect">
					<i class="icon-paper-clip"></i><span>MY PROFILE</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="sidebar-submenu">
					<li><a href="<?php echo base_url(); ?>account/info"><i class="fa fa-long-arrow-right"></i>Acc.Information</a></li>
					<li><a href="<?php echo base_url(); ?>view_tpin"><i class="fa fa-long-arrow-right"></i> View Account T-Pin</a></li>
					<li><a href="<?php echo base_url(); ?>change_tpin"><i class="fa fa-long-arrow-right"></i> Change Account T-Pin</a></li>
					<li><a href="<?php echo base_url(); ?>change_pasword"><i class="fa fa-long-arrow-right"></i> Change Account Password</a></li>
				</ul>
			</li>

			<li>
				<a href="javaScript:void();" class="waves-effect">
					<i class="icon-paper-clip"></i><span>MY NETWORK</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="sidebar-submenu">
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i>New Member</a></li>
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i> My Referrals</a></li>
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i> My Genelogy Tree</a></li>
				</ul>
			</li>

			<li>
				<a href="javaScript:void();" class="waves-effect">
					<i class="icon-paper-clip"></i><span>FINANCIAL</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="sidebar-submenu">
					<li><a href="<?php echo base_url(); ?>add-fund"><i class="fa fa-long-arrow-right"></i>Add Funds</a></li>
					<li><a href="<?php echo base_url(); ?>fund/transfer"><i class="fa fa-long-arrow-right"></i>Fund Transfer</a></li>
					<li><a href="<?php echo base_url('withdraw/'); ?>"><i class="fa fa-long-arrow-right"></i>Withdraw History</a></li>
					<li><a href="<?php echo base_url(); ?>withdraw/add"><i class="fa fa-long-arrow-right"></i>Withdraw</a></li>
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i> Income Statement</a></li>
						<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i> Expense Statement</a></li>
				</ul>
			</li>

			<li>
				<a href="javaScript:void();" class="waves-effect">
					<i class="icon-paper-clip"></i><span>BOARD</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="sidebar-submenu">
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i>1st Boards</a></li>
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i>2nd Board</a></li>
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i>3rd Board</a></li>
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i>4th Board</a></li>
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i>5th Board</a></li>
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i>6th Board</a></li>
				</ul>
			</li>
			<li>
				<a href="javaScript:void();" class="waves-effect">
					<i class="icon-paper-clip"></i><span>Product</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="sidebar-submenu">
					<li><a href="<?php echo base_url(); ?>products"><i class="fa fa-long-arrow-right"></i>Product List</a></li>

				</ul>
			</li>

			<li>
				<a href="javaScript:void();" class="waves-effect">
					<i class="icon-paper-clip"></i><span>History</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="sidebar-submenu">
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-long-arrow-right"></i>Purchase Product History</a></li>

				</ul>
			</li>
			<li>
				<a href="javaScript:void();" class="waves-effect">
					<i class="icon-paper-clip"></i><span>Support</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="sidebar-submenu">
					<li><a href="<?php echo base_url(); ?>sub/create"><i class="fa fa-long-arrow-right"></i>Add</a></li>
					<li><a href="<?php echo base_url(); ?>sub/list"><i class="fa fa-long-arrow-right"></i> List</a></li>
				</ul>
			</li>

			<li>
				<a href="javaScript:void();" class="waves-effect">
					<i class="icon-paper-clip"></i><span>sponsor</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="sidebar-submenu">

					<li><a href="<?php echo base_url(); ?>user/list"><i class="fa fa-long-arrow-right"></i> sponsor history</a></li>
				</ul>
			</li>
		</ul>

	</div>
	<!--End sidebar-wrapper-->

	<!--Start topbar header-->
	<header class="topbar-nav">
		<nav class="navbar navbar-expand fixed-top gradient-ibiza">
			<ul class="navbar-nav mr-auto align-items-center">
				<li class="nav-item">
					<a class="nav-link toggle-menu" href="javascript:void();">
						<i class="icon-menu menu-icon"></i>
					</a>
				</li>
				<li class="nav-item">
					<form class="search-bar">
						<input type="text" class="form-control" placeholder="Enter keywords">
						<a href="javascript:void();"><i class="icon-magnifier"></i></a>
					</form>
				</li>
			</ul>

			<ul class="navbar-nav align-items-center right-nav-link">
				<li class="nav-item dropdown-lg">
					<a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">

					<div class="dropdown-menu dropdown-menu-right animated fadeIn">
						<ul class="list-group list-group-flush">






						</ul>
					</div>
				</li>

				<li class="nav-item language">
					<a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="#"></a>
					<ul class="dropdown-menu dropdown-menu-right animated fadeIn">

					</ul>
				</li>
				<li class="nav-item">
					<a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
						<span class="user-profile"><img src="<?php echo base_url(); ?>ui/backend/images/avatars/avatar-17.png" class="img-circle" alt="user avatar"></span>
					</a>
					<ul class="dropdown-menu dropdown-menu-right animated fadeIn">
						<li class="dropdown-item user-details">
							<a href="javaScript:void();">
								<div class="media">
									<div class="avatar"><img class="align-self-start mr-3" src="<?php echo base_url(); ?>ui/backend/images/avatars/avatar-17.png" alt="user avatar"></div>
									<div class="media-body">
										<h6 class="mt-2 user-title"><?php echo $_SESSION['firstname']; ?></h6>

									</div>
								</div>
							</a>
						</li>
						<li class="dropdown-divider"></li>
						<li class="dropdown-item"><i class="icon-envelope mr-2"></i> Inbox</li>
						<li class="dropdown-divider"></li>
						<li class="dropdown-item"><i class="icon-wallet mr-2"></i> Account</li>
						<li class="dropdown-divider"></li>
						<li class="dropdown-item"><i class="icon-settings mr-2"></i> Setting</li>
						<li class="dropdown-divider"></li>
						<li class="dropdown-item"><a href="<?php echo base_url()?>user_login/user/user_logout"><i class="icon-power"></i> Logout</a></li>


					</ul>
				</li>
			</ul>
		</nav>
	</header>
	<!--End topbar header-->

	<div class="clearfix"></div>
