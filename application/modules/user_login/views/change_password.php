<?php
$this->load->view('/layout/header');
?>
<div class="content-wrapper">
  <div class="container-fluid">

    <!--Start Dashboard Content-->

    <div class="card-group">
      <div class="card">

        <div class="card-header">
            <h5 class="text-center">CHANGE PASSWORD</h5>
        </div>

        <div class="card-body">
          <?php if(isset($_SESSION['success']))
          {
          ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <div class="alert-icon">
                <i class="icon-check"></i>
              </div>
              <div class="alert-message">
                <?php echo $this->session->flashdata('success'); ?>
              </div>
            </div>
          <?php
          }
          ?>

          <?php if(isset($_SESSION['errorMessage']))
          {
            ?>


            <div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <div class="alert-icon">
                <i class="icon-exclamation"></i>
              </div>
              <div class="alert-message">
                <?php echo $this->session->flashdata('errorMessage'); ?>
              </div>
            </div>
            <?php
          }
          ?>

          <form method="post" action="<?php echo base_url()?>password/update" enctype="multipart/form-data">

            <div class="form-group row">
              <label for="basic-input" class="col-sm-3 col-form-label">Old Password :</label>
              <div class="col-sm-9">
                <input type="password" value="<?php echo set_value('old_password')?>"  name="old_password" id="basic-input" class="form-control">
                <span id="input-14-error" class="error"><?php echo form_error('old_password'); ?></span>
              </div>
            </div>

              <div class="form-group row">
              <label for="basic-input" class="col-sm-3 col-form-label">New Password :</label>
              <div class="col-sm-9">
                <input type="password" value="<?php echo set_value('new_password')?>" name="new_password" id="basic-input" class="form-control">
                <span id="input-14-error" class="error"><?php echo form_error('new_password'); ?></span>
              </div>
              </div>

              <div class="form-group row">
              <label for="basic-input" class="col-sm-3 col-form-label">Confirm Password :</label>
              <div class="col-sm-9">
                <input type="password" value="<?php echo set_value('c_password')?>" name="c_password" id="basic-input" class="form-control">
                <span id="input-14-error" class="error"><?php echo form_error('c_password'); ?></span>
              </div>
              </div>

              <div class="form-group row">
              <label for="basic-input" class="col-sm-3 col-form-label">T-Pin :</label>
              <div class="col-sm-9">
                <input type="text" value="<?php echo set_value('t_pin')?>" name="t_pin" id="basic-input" class="form-control">
                <span id="input-14-error" class="error"><?php echo form_error('t_pin'); ?></span>
              </div>
              </div>

            <div class="form-footer row">
              <div class="col-sm-10">
                <p> Don't share your password with others.</p>
              </div>
              <div class="col-sm-2">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Update</button>
              </div>


            </div>

          </form>
        </div>

        </div>

      </div>
    </div>


    <!--End Dashboard Content-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper
<?php
$this->load->view('/layout/footer');
?>
