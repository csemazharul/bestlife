<?php
$this->load->view('/layout/header');
?>
<div class="content-wrapper">
  <div class="container-fluid">

    <!--Start Dashboard Content-->

    <div class="card-group">
      <div class="card">


        <div class="card-body">
            <h3 class="text-center">ACCOUNT OVERVIEW</h3>

        </div>

      </div>
    </div>

    <div class="row mt-4">
      <div class="col-12 col-lg-6 col-xl-6">

          <div class="card text-center">
              <div class="card-header">
                <h3>Member's Info</h3>
              </div>
              <div class="card-body">
                <h4 class="text-center">Md Sahed Islam</h4>
                <table class="table table-hover">

                  <tr><td>Member ID #      :</td><td>12309</td></tr>
                  <tr><td>Contact Number   :</td><td>01988654321</td></tr>
                  <tr><td>Account Balance  :</td><td>150.00 BDT</td></tr>
                  <tr><td>Registration Date:</td><td>10/09/2019</td></tr>
                  <tr><td>Membership Status:</td><td><a href="" class="btn btn-primary btn-sm">Active</a> <a href="" class="btn btn-primary btn-sm">Verified</a></td></tr>

              </table>
              </div>
              <!-- <div class="card-footer text-muted">
                2 days ago
              </div> -->

        </div>
      </div>

      <div class="col-12 col-lg-6 col-xl-6">

          <div class="card text-center">
              <div class="card-header">
                <h3>Sponsor's Info</h3>
              </div>
              <div class="card-body">
                <h4 class="text-center">Md Arif Uddin</h4>
                <table class="table table-hover">

                  <tr><td>Member ID #      :</td><td>1298</td></tr>
                  <tr><td>Contact Number   :</td><td>01879134451</td></tr>
                  <tr><td>Account Balance  :</td><td>100.00 BDT</td></tr>
                  <tr><td>Registration Date:</td><td>10/10/2019</td></tr>
                  <tr><td>Membership Status:</td><td><a href="" class="btn btn-primary btn-sm">Active</a> <a href="" class="btn btn-primary btn-sm">Verified</a></td></tr>

              </table>
              </div>
              <!-- <div class="card-footer text-muted">
                2 days ago
              </div> -->

        </div>
      </div>

    </div>



    <!--End Dashboard Content-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper
<?php
$this->load->view('/layout/footer');
?>
