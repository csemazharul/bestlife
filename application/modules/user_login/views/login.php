<?php
$this->load->view('frontend/layout/header');
?>
<div class="breadcrumb_wrapper">

		<div class="breadcrumb_block">
				<ul>
						<li><a href="">home</a></li>
						<li>Login</li>
				</ul>
		</div>
</div>

<!--Contact Form-->
<div class="contact_form_wrapper clv_section">
		<div class="container">
				<div class="row">
						<div class="col-lg-6 col-md-8 offset-lg-3">
								<div class="contact_form_section">
										<div class="row">
											<?php if(isset($_SESSION['message']))
											{
											?>
											<div class="alert alert-success" style="margin-left:100px" id="message">
												 <?php echo $this->session->flashdata('message'); ?>
											</div>
											<?php
											}
											?>
												<div class="col-md-12 col-lg-12">
														<h3>Login</h3>
														<h6></h6>
												</div>

					<form action="<?php echo base_url() ?>user/login/store" method="post">

												<div class=" col-md-6 col-lg-6">
														<div class="form_block">
																<input type="email" name="email"  class=" form_field require" placeholder="Email" data-valid="email" data-error="" >
																<span id="input-14-error" class="" style="color:red;"><?php echo form_error('email'); ?></span>
														</div>
												</div>

												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<input type="password" name="password"  class=" form_field require" placeholder="Password" data-valid="password" data-error="" >
																<span id="input-14-error" class="" style="color:red;"><?php echo form_error('password'); ?></span>
														</div>
												</div>


												<div class="col-md-12 col-lg-12">
														<div class="form_block">
																<button type="submit" class="clv_btn submitForm">Submit</button>
														</div>
												</div>
					</form>
										</div>
								</div>
						</div>

				</div>
		</div>
</div>

<?php
$this->load->view('frontend/layout/footer');
?>
