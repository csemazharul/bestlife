<?php
class User_model extends CI_model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function register_user($user){
		$this->db->insert('login', $user);

	}

	public function login_user($email,$pass){
		//$email,$pass


		$this->db->select('*');
		$this->db->from('login');
		$this->db->where('email',$email);
		$this->db->where('password',$pass);
		$this->db->where('verify_status',1);
		$query=$this->db->get();

		if($query)
		{
			return $query->result_array();
		}
		else{
			return false;
		}


	}

	public function verified_check($emailVerified){

		$this->db->select('*');
		$this->db->from('login');
		$this->db->where('verify_status',$emailVerified);
		 $query=$this->db->get();

		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}

	}
		public function userList()
	{
		$query = $this->db->get('login');
		return $query->result();
	}
    public function sponserCheck($sponser_id)
	{
		$this->db->select('*');
		$this->db->from('login');
		$this->db->where('sponser_id',$sponser_id);
		$query=$this->db->get();
		return $query->num_rows();
	}

	public function addfundInsert($addfund)
	{
		$this->db->insert('fund_add_req', $addfund);
	}

	public function checkTpin($tpin)
	{
		$this->db->select('*');
		$this->db->from('login');
		$this->db->where('id',$_SESSION['user_id']);
		$this->db->where('t_pin',$tpin);
		$query=$this->db->get();

		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}

	}
	public function passwordCheck($password)
	{
		$this->db->select('*');
		$this->db->from('login');
		$this->db->where('password',$password);
		$this->db->where('id',$_SESSION['user_id']);
		$query=$this->db->get();

		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}

	}



}


?>
