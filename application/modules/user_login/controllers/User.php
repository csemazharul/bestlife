<?php


class User extends MX_Controller
{
	public function __construct(){

		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->model('user_model');
		$this->load->model('fund/fund_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}

	public function register()
	{
			$this->load->view('/register');

	}

		public function register_user()
	{

		if($_POST['country_code']=='0')
		{
			$_POST['country_code']='';
		}
		if($_POST['currency']=='0')
		{
				$_POST['currency']='';
		}

		$data = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules('email', 'email id', 'required|is_unique[login.email]');
		$this->form_validation->set_rules('email', 'email', 'required|is_unique[login.email]|valid_email',
        array('is_unique' => 'Email has already been taken','valid_email'=>'Please provide a valid email address.')
				);
		$this->form_validation->set_rules('country_code', 'country code', 'required');
		$this->form_validation->set_rules('mobile_no', 'mobile no', 'required');
		$this->form_validation->set_rules('firstname', 'first name', 'required');
		$this->form_validation->set_rules('lastname', 'last name', 'required');
		$this->form_validation->set_rules('date_of_birth', 'date of birth', 'required');
		$this->form_validation->set_rules('currency', 'currency', 'required');
		$this->form_validation->set_rules('fullname', 'fullname', 'required');
		$this->form_validation->set_rules('username', 'username', 'required|is_unique[login.username]',
				array('is_unique' => 'Username has already been taken')
				);
		$this->form_validation->set_rules('user_id', 'user id', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('confirm_password', 'confirm password', 'required|matches[password]');
		$this->form_validation->set_rules('t_pin', 't pin', 'required');
		$this->form_validation->set_rules('tre_pin', 'tre_pin', 'required');
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('sponser_id', 'sponser_id', 'required');

	  $countSponserId=$this->user_model->sponserCheck($_POST['sponser_id']);

		if($countSponserId>=4){
			$_POST['sponser_id']='';
			$this->form_validation->set_rules('sponser_id', 'sponser_id', 'required',
			array('required'=>'
				a sponser id cannot be used more than four'));
		}

		if ($this->form_validation->run())
		{

				$data['success'] = true;
				$user=array(
					'email'=>$this->input->post('email'),
					'sponser_id'=>$this->input->post('sponser_id'),
					'country_code'=>$this->input->post('country_code'),
					'mobile_no'=>$this->input->post('mobile_no'),
					'firstname'=>$this->input->post('firstname'),
					'lastname'=>$this->input->post('lastname'),
					'date_of_birth'=>$this->input->post('date_of_birth'),
					'currency'=>$this->input->post('currency'),
					'fullname'=>$this->input->post('fullname'),
					'username'=>$this->input->post('username'),
					'user_id'=>$this->input->post('user_id'),
					'password'=>md5($this->input->post('password')),
					't_pin'=>$this->input->post('t_pin'),
					'tre_pin'=>$this->input->post('tre_pin'),
					'created_at'=>date('Y-m-d h:i:a'),
					'updated_at'=>date('Y-m-d h:i:a'),
					'verify_status'=>md5(rand(1,100000)),
				);
				$this->user_model->register_user($user);
				$this->sendEmail($user['verify_status'],$user['email']);
				$this->session->set_flashdata('message', '<strong>Thank you!</strong>Your Mail has been sent successfully.</span>');

		}

		else {

			foreach ($_POST as $key => $value)
			{

				 $data['messages'][$key] = form_error($key);
			}
		}
		echo json_encode($data);

	}
	public function sendEmail($token,$email)
	{
		 $this->load->library('email');
		 $config = array();
	  	$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'smtp.mailgun.org';
			$config['smtp_user'] = 'postmaster@test.myscoresme.com';
			$config['smtp_pass'] = '20c4b1191b6822c9ac9655d9d0bb1905-41a2adb4-7f832e5d';
			$config['smtp_port'] = 587;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			$this->email->from('postmaster@test.myscoresme.com', 'Best Life Limited');
		 $this->email->to($email);
		 $this->email->subject('Email Verification');
		 $message = "
			 Please click this link to verify your account:
			 http://floricbd.com/best_life_limited/user/verified/".$token;

		$this->email->message($message);

		$this->email->send();

	}
	public function verified()
	{
		$verified = $this->uri->segment(3);

		$check=$this->user_model->verified_check($verified);
		if($check)
		{
			$this->db->set('verify_status',1); //value that used to update column
			$this->db->where('verify_status', $verified); //which row want to upgrade
			$status=$this->db->update('login');  //table name
			if($status)
			{
				$this->session->set_flashdata('message', '<strong>Success!</strong>Your email verified..please login now.</span>');
				redirect('/user/login');
			}
		}
	}

	public function loginForm()
	{

		if(isset($_SESSION['email']))
		{

		return redirect('user/dashboard');
		}
		else
		{

			$this->load->view('login');

		}

	}

	public function login_user(){
		$user_login=array(

			'email'=>$this->input->post('email'),
			'password'=>md5($this->input->post('password'))

		);
//$user_login['user_email'],$user_login['user_password']
		$data['users']=$this->user_model->login_user($user_login['email'],$user_login['password']);
        //print_r($data['users']);
        //echo "</br>";
		if($data['users'])
		{
			$this->session->set_userdata('user_id',$data['users'][0]['id']);
			$this->session->set_userdata('email',$data['users'][0]['email']);
			$this->session->set_userdata('firstname',$data['users'][0]['firstname']);
			$this->session->set_userdata('username',$data['users'][0]['username']);


			redirect('/user/dashboard', 'refresh');

		}
		else{
			$this->session->set_flashdata('message', 'Wrong Information please again try again.');
			$this->load->view("/login.php");

		}


	}

	public function user_logout(){

	  	$this->session->sess_destroy();
			$this->session->set_flashdata('message', 'Your account logout successfully.');
		$this->load->view("/login.php");
	}
	public function userDashboard()
	{
		if(isset($_SESSION['email']))
		{
			$user=$this->fund_model->amountCheck($_SESSION['user_id']);
			$balance['balance']=$user->c_balance;
			$this->load->view("/user_dashboard",$balance);

		}
		else
		{
			redirect('/user/login');

		}

	}


	public function userList()
	{
		$data['users']=$this->user_model->userList();
		$this->load->view('userlist',$data);

	}


	public function addFund()
	{
		$this->load->view('add_fund');

	}

	public function fundStore()
	{
		$this->form_validation->set_rules('amount', 'amount', 'required');
		$this->form_validation->set_rules('t_pin', 'tpin', 'required');

		$tpin=$this->input->post('t_pin');
		$checkTpin = $this->user_model->checkTpin($tpin);
			if ($this->form_validation->run())
			{
				if($checkTpin)
				{

					$addfund['user_id'] = $_SESSION['user_id'];
					$addfund['request_name'] = $_SESSION['firstname'];
					$addfund['amount'] = $this->input->post('amount');
					$addfund['t_pin'] = $this->input->post('t_pin');
					$addfund['date'] = date('Y-m-d');
					$addfund['date_time'] = date('Y-m-d h:i:a');

					$query = $this->user_model->addfundInsert($addfund);
					$this->session->set_flashdata('message', '<span><strong>Success!</strong> Fund Add Request Send Done.</span>');
					$this->load->view('add_fund');
				}
				else
				{
					$this->session->set_flashdata('errorMessage', '<span><strong>Warning!</strong> your tpin does not match.</span>');
					$this->load->view("add_fund");
				}

			}
			else
			{
				$this->load->view("add_fund");
			}

		}

		public function accountInfo()
		{
			$this->load->view('account_info');
		}

		public function viewTpin()
		{
			$this->load->view('view_tpin');
		}

		public function changeTpin()
		{
			$this->load->view('change_tpin');
		}

		public function tpinUpdate()
		{
			$password=$this->input->post('password');
			$tpin=$this->input->post('old_tpin');

			$status=$this->user_model->passwordCheck(md5($password));
			$checkTpin = $this->user_model->checkTpin($tpin);

			$this->form_validation->set_rules('old_tpin', 'old tpin', 'required');
			$this->form_validation->set_rules('t_pin', 't_pin', 'required|is_unique[login.t_pin]');
			$this->form_validation->set_rules('c_tpin', 'confirm tpin', 'required|matches[t_pin]');
			$this->form_validation->set_rules('password', 'password', 'required');

			if ($this->form_validation->run())
			{
				if($status==1 && $checkTpin==1 )
				{

					 $this->db->set('t_pin',$this->input->post('t_pin')); //value that used to update column
				  	$this->db->where('id', $_SESSION['user_id']); //which row want to upgrade
					 $this->db->update('login');  //table name
					 $this->session->set_flashdata('success', 't-pin update successfully!.');
					 redirect('change_tpin','refresh');
				}
				else {

					$this->session->set_flashdata('errorMessage', 'Wrong Information!! old t-pin or account password is Incorrect.');
					$this->load->view("change_tpin");
				}
			}
			else {
				$this->load->view('change_tpin');
			}

		}

		public function chnagePassword()
		{
			$this->load->view('change_password');
		}
		public function passwordUpdate()
		{

			$oldpassword=$this->input->post('old_password');
			$tpin=$this->input->post('t_pin');

			$status=$this->user_model->passwordCheck(md5($oldpassword));
			$checkTpin = $this->user_model->checkTpin($tpin);

			$this->form_validation->set_rules('old_password', 'old tpin', 'required');
			$this->form_validation->set_rules('new_password', 't_pin', 'required');
			$this->form_validation->set_rules('c_password', 'confirm tpin', 'required|matches[new_password]');
			$this->form_validation->set_rules('t_pin', 't pin', 'required');

			if ($this->form_validation->run())
			{
				if($status==1 && $checkTpin==1 )
				{

					 $this->db->set('password',md5($this->input->post('new_password'))); //value that used to update column
						$this->db->where('id', $_SESSION['user_id']); //which row want to upgrade
					 $this->db->update('login');  //table name
					 $this->session->set_flashdata('success', 'password update successfully!.');
					 redirect('change_pasword','refresh');
				}
				else {

					$this->session->set_flashdata('errorMessage', 'Wrong Information!! old t-pin or old password is Incorrect.');
					$this->load->view("change_password");
				}
			}
			else {
				$this->load->view('change_password');
			}

		}

		public function tpinShow()
		{
			$oldpassword=$this->input->post('old_password');

			$status=$this->user_model->passwordCheck(md5($oldpassword));
			if($status==1)
			{

				$this->db->select('t_pin');
				$this->db->from('login');
				$this->db->where('id',$_SESSION['user_id']);
				$query=$this->db->get();
				$data['tpin']=$query->first_row();
				$this->load->view('tpin_show',$data);

			}
			else {
				$this->session->set_flashdata('errorMessage', 'Wrong Information!! Your Password does not match.');
				$this->load->view('view_tpin');
			}

		}

}
