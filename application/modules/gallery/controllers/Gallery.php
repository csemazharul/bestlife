<?php


class Gallery extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
    $this->load->model('Gallery_model');
		$this->load->library('session');

	}

	public function galleryCreate()
	{

		$this->load->view('gallery/create.php');
	}

	public function galleryList()
	{
	 $gallery['galleries']=$this->Gallery_model->getAllgallery();



		$this->load->view('gallery/list.php',$gallery);
	}

	public function galleryStore()
	{
		if(!empty($_FILES['picture']['name']))
		{
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['picture']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('picture'))
			{
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture = '';
			}
		}else{
			$picture = '';
		}

		$gallery['title'] = $this->input->post('title');
		$gallery['short_description'] = $this->input->post('short_description');
		$gallery['picture'] = $picture;
		$gallery['created_at'] =date('Y-m-d h:i:a');
		$gallery['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->Gallery_model->galleryInsert($gallery);

		redirect('gallery/list','refresh');

	}

	public function getGallery()
	{
		$id = $this->uri->segment(3);

		$gallery['gallery'] = $this->Gallery_model->getgallery($id);
		$this->load->view('gallery/edit',$gallery);
	}

	public function galleryUpdate()
	{

		$id = $this->uri->segment(3);

		$galleryData= $this->Gallery_model->getgallery($id);

		if(!empty($_FILES['picture']['name'])){
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['picture']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('gallery')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture ='';
			}
		}else{
			$picture =  $galleryData['picture'];
		}
		$gallery['title'] = $this->input->post('title');
		$gallery['short_description'] = $this->input->post('short_description');
		$gallery['picture'] = $picture;
		$gallery['created_at'] =$galleryData['created_at'];
		$gallery['updated_at'] =date('Y-m-d h:i:a');


		$query = $this->Gallery_model->galleryUpdate($gallery,$id);

		redirect('gallery/list','refresh');

	}

	public function delete()
	{
		
		$id = $this->uri->segment(3);
		$query = $this->Gallery_model->deletegallery($id);

		redirect('gallery/list','refresh');
	}


}
