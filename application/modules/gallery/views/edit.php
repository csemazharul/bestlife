Hi
<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header text-uppercase">Gallery edit</div>
					<div class="card-body">

						<form method="post" action="<?php echo base_url('gallery/update/'.$gallery['id'])?>" enctype="multipart/form-data">

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Title</label>
								<div class="col-sm-9">
									<input type="text" name="title" value="<?php echo $gallery['title']?>" id="basic-input" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">Short Description</label>
								<div class="col-sm-9">
									<textarea rows="8" name="short_description" class="form-control" id="basic-textarea"><?php echo $gallery['short_description']?></textarea>
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Picture</label>
								<div class="col-sm-9">
									<input type="file" name="picture" class="form-control"><br>
                  <img src="<?php echo base_url('upload/images/'.$gallery['picture'])?>" width="150px" height="120px"/>
								</div>

							</div>

							<button type="submit" class="btn btn btn-success shadow btn-block ">Update</button>

						</form>

					</div>
				</div>
			</div>
		</div><!--End Row-->
	</div>
	<!-- End container-fluid-->


</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
