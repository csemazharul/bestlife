<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><i class="fa fa-table"></i>Gallery List</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="default-datatable" class="table table-bordered">
								<thead>
								<tr>
									<th>Id</th>
									<th>title</th>
									<th>Short Description</th>
									<th>picture</th>
									<th>Action</th>

								</tr>
								</thead>
								<tbody>
								<?php
								foreach($galleries as $gallery){
									?>
									<tr>
										<td><?php echo $gallery->id; ?></td>
										<td><?php echo $gallery->title; ?></td>
										<td><?php echo $gallery->short_description; ?></td>
										<td><img src="<?php echo base_url(); ?>upload/images/<?php echo $gallery->picture; ?>" width="100px" height="80px"/></td>

										<td><a href="<?php echo base_url(); ?>gallery/edit/<?php echo $gallery->id; ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="<?php echo base_url(); ?>gallery/delete/<?php echo $gallery->id; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</a></td>
									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
