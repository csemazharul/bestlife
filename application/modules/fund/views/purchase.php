<?php
$this->load->view('user_login/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					<div class="card-header text-uppercase">Transfer Fund</div>
					<div class="card-body">

						<?php if(isset($_SESSION['message']))
						{
						?>
						<div class="alert alert-success" id="message">
							 <h4><?php echo $this->session->flashdata('message'); ?></h4>
						</div>
						<?php
						}
						?>

						<form method="post" action="<?php echo base_url()?>purchase/store" enctype="multipart/form-data">

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Select product</label>
								<div class="col-sm-9">
									<select id="receiver_id"  name="receiver_id" class="form-control">
										<option disabled selected>Select Account Name</option>
										<?php
										foreach($users as $user) {
											?>

											<option value="<?php echo $user->id?>"><?php echo $user->username?></option>
											<?php
										}
										?>
									</select>

								</div>
							</div>


							<input type="hidden" name="">
							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">amount</label>
								<div class="col-sm-9">
									<input type="text" name="amount" id="basic-input" class="form-control">
								</div>
							</div>

              <div class="form-group row">
                <label for="basic-input" class="col-sm-3 col-form-label">tpin</label>
                <div class="col-sm-9">
                  <input type="text" name="t_pin" id="basic-input" class="form-control">
                </div>
              </div>

							<button type="submit" class="btn btn btn-primary shadow btn-block ">Submit</button>

						</form>

					</div>
				</div>
			</div>
		</div><!--End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper


<?php
$this->load->view('user_login/layout//footer');
?>
