<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><i class="fa fa-table"></i> Fund History</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="default-datatable" class="table table-bordered">
								<thead>
								<tr>
									<th>Requet ID</th>
									<th>Request Name</th>
									<th>Amount</th>
									<th>Amount withdraw</th>
									<th>Admin Charge</th>
									<th>status</th>
									<th>Approve</th>
									<th>approved_at</th>


								</tr>
								</thead>
								<tbody>
								<?php
								foreach($allhistory as $history){
									?>
									<tr>
										<td><?php echo $history->withdraw_user_id; ?></td>
										<td><?php echo $history->request_name; ?></td>
										<td><?php echo $history->amount; ?></td>
										<td><?php echo $history->amount_withdraw; ?></td>
										<td><?php echo $history->admin_charge; ?></td>


										<td>
											<?php

												if($history->fund_trn_st==0)
												{
														 echo "Pending";
												}
												else {
													echo "Approved";
												}

											?>
		                </td>
											<td>
												<?php
												if($history->fund_trn_st==0)
												{
												?>
													<a href="<?php echo base_url(); ?>withdraw/status/<?php echo $history->withdraw_id.'/'.$history->amount_withdraw.'/'. $history->withdraw_user_id; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Approve </a>
												<?php
												}

												if($history->fund_trn_st==1)
												{
												?>
												<a href="<?php echo base_url(); ?>withdraw/status/<?php echo $history->withdraw_id; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Not Approved </a>
												<?php
											  }
												 ?>



											</td>

                      <td>
                        <?php echo $history->approved_at?>
                      </td>


									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
