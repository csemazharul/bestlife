<?php
$this->load->view('user_login/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><i class="fa fa-table"></i> product List</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="default-datatable" class="table table-bordered">
								<thead>
								<tr>
									<th>Id</th>
									<th>title</th>
									<th>picture</th>
									<th>point</th>
									<th>Amount</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								<?php
								foreach($products as $product){
									?>
									<tr>
										<td><?php echo $product->id; ?></td>
										<td><?php echo $product->title; ?></td>
										<td><img src="<?php echo base_url(); ?>upload/images/<?php echo $product->picture; ?>" width="100px" height="80px"/></td>
										<td><?php echo $product->point; ?></td>
										<td><?php echo $product->price; ?></td>

										<td><a href="<?php echo base_url(); ?>add_purchase/<?php echo $product->id; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Purchase</a></td>
									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

  <?php
  $this->load->view('user_login/layout/footer');
  ?>
