<?php
$this->load->view('user_login/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					<div class="card-header text-uppercase">Transfer Fund</div>
					<div class="card-body">

						<?php if(isset($_SESSION['success']))
						{
						?>
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<div class="alert-icon">
									<i class="icon-check"></i>
								</div>
								<div class="alert-message">
									<?php echo $this->session->flashdata('success'); ?>
								</div>
							</div>
						<?php
						}
						?>

						<?php if(isset($_SESSION['errorMessage']))
						{
						?>
							<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<div class="alert-icon">
									<i class="icon-exclamation"></i>
								</div>
								<div class="alert-message">
									<?php echo $this->session->flashdata('errorMessage'); ?>
								</div>
							</div>

						<?php
						}
						?>


						<form method="post" action="<?php echo base_url()?>transferfund/store" enctype="multipart/form-data">

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Select Acouunt Name</label>
								<div class="col-sm-9">
									<select id="receiver_id"  name="receiver_id" class="form-control">
										<option disabled selected>Select Account</option>
										<?php
										foreach($users as $user) {
											?>

											<option value="<?php echo $user->id?>" <?php echo set_select('receiver_id',$user->id, ( !empty($data) && $data == $user->id ? TRUE : FALSE )); ?>><?php echo $user->username?></option>
											<?php
										}
										?>

									</select>
									<span id="input-14-error" class="error"><?php echo form_error('receiver_id'); ?></span>

								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">amount</label>
								<div class="col-sm-9">
									<input type="text" value="<?php echo set_value('amount'); ?>" name="amount" id="input-14-error" class="form-control">
									<span id="input-14-error" class="error"><?php echo form_error('amount'); ?></span>
								</div>
							</div>

						  <div class="form-group row">
							<label for="basic-input" class="col-sm-3 col-form-label">tpin</label>
							<div class="col-sm-9">
							  <input type="text" name="t_pin"  value="<?php echo set_value('t_pin'); ?>" id="basic-input" class="form-control">
								<span id="input-14-error" class="error"><?php echo form_error('t_pin'); ?></span>
							</div>
						  </div>

							<div class="form-footer">

								<button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Submit</button>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div><!--End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper


<?php
$this->load->view('user_login/layout//footer');
?>
