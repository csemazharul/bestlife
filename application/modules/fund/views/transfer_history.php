<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><i class="fa fa-table"></i> Fund History</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="default-datatable" class="table table-bordered">
								<thead>
								<tr>
									<th>Sender ID</th>
									<th>Sender Name</th>
									<th>Amount</th>
									<th>Date</th>

								</tr>
								</thead>
								<tbody>
								<?php
								foreach($allhistory as $history){
									?>
									<tr>
										<td><?php echo $history->sender_id; ?></td>
										<td><?php echo $history->sender_name; ?></td>
										<td><?php echo $history->amount; ?></td>
										<td><?php echo $history->tr_date; ?></td>
				
									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
