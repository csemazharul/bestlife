<?php
$this->load->view('user_login/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					<div class="card-header text-uppercase">purchase Product</div>
					<div class="card-body">

						<?php if(isset($_SESSION['success']))
						{
						?>
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<div class="alert-icon">
									<i class="icon-check"></i>
								</div>
								<div class="alert-message">
									<?php echo $this->session->flashdata('success'); ?>
								</div>
							</div>
						<?php
						}
						?>

						<?php if(isset($_SESSION['errorMessage']))
						{
						?>
							<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<div class="alert-icon">
									<i class="icon-exclamation"></i>
								</div>
								<div class="alert-message">
									<?php echo $this->session->flashdata('errorMessage'); ?>
								</div>
							</div>

						<?php
						}
						?>


						<form method="post" action="<?php echo base_url()?>purchase/store" enctype="multipart/form-data">
							<input type="hidden" name="product_id" value="<?php echo $productInfo['id']?>">
							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Product Name</label>
								<div class="col-sm-9">
									<input type="text" value="<?php echo $productInfo['title'] ?>"  name="product_name" id="input-14-error" class="form-control" readonly>
									<span id="input-14-error" class="error"><?php echo form_error('product'); ?></span>
								</div>
							</div>

						  <div class="form-group row">
							<label for="basic-input" class="col-sm-3 col-form-label">Amount</label>
							<div class="col-sm-9">
							  <input type="text"  name="price"  value="<?php echo $productInfo['price'] ?>"  id="basic-input" class="form-control" readonly>

							</div>
						  </div>

              <div class="form-group row">
							<label for="basic-input" class="col-sm-3 col-form-label">Point</label>
							<div class="col-sm-9">
							  <input type="text" name="point"  value="<?php echo $productInfo['point'] ?>" disabled id="basic-input" class="form-control" readonly>
								
							</div>
						  </div>

							<div class="form-footer">
                                <a href="<?php echo base_url('products')?>"  class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</a>
								<button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Submit</button>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div><!--End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper


<?php
$this->load->view('user_login/layout//footer');
?>
