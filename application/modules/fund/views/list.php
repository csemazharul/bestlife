<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><i class="fa fa-table"></i> Fund History</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="default-datatable" class="table table-bordered">
								<thead>
								<tr>
									<th>Requet ID</th>
									<th>Request Name</th>
									<th>Amount</th>
									<th>Date</th>
									<th>Status</th>
									<th>Approve</th>


								</tr>
								</thead>
								<tbody>
								<?php
								foreach($allfund as $fund){
									?>
									<tr>
										<td><?php echo $fund->user_id; ?></td>
										<td><?php echo $fund->request_name; ?></td>
										<td><?php echo $fund->amount; ?></td>
										<td><?php echo $fund->date; ?></td>
										<td>
											<?php

												if($fund->fund_trn_st==0)
												{
														 echo "Pending";
												}
												else {
													echo "Approved";
												}

											?>
		                </td>
											<td>
												<?php
												if($fund->fund_trn_st==0)
												{
												?>
													<a href="<?php echo base_url(); ?>fund/status/<?php echo $fund->id.'/'.$fund->amount.'/'. $fund->user_id; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Approve </a>
												<?php
												}

												if($fund->fund_trn_st==1)
												{
												?>
												<a href="<?php echo base_url(); ?>fund/status/<?php echo $fund->id; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Not Approved </a>
												<?php
											  }
												 ?>



											</td>

									</tr>
									<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
