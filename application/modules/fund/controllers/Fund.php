<?php


class Fund extends MX_Controller
{
	public function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->load->model('fund_model');
		$this->load->model('user_login/user_model');
		$this->load->model('product/Product_model');
		$this->load->library('session');
		$this->load->library('form_validation');

	}

	public function fundList()
	{
		$fund['allfund']=$this->fund_model->getAllFund();

		$this->load->view('fund/list.php',$fund);
	}

	public function fundStatus()
	{
		$fund_id = $this->uri->segment(3);
		$amount = $this->uri->segment(4);
		$user_id = $this->uri->segment(5);


		$query = $this->db->query('SELECT * FROM login WHERE id='.$user_id);
	  $row = $query->first_row();
	  $totalamount=$row->c_balance+$amount;

		$this->db->set('c_balance',$totalamount); //value that used to update column
		$this->db->where('id', $user_id); //which row want to upgrade
		$status=$this->db->update('login');

		$this->db->set('fund_trn_st',1); //value that used to update column
		$this->db->where('id', $fund_id); //which row want to upgrade
		$status=$this->db->update('fund_add_req');  //table name

		redirect("fund/list","refresh");

	}

	public function addTransferFund()
	{
		$id = $_SESSION['user_id'];
		$this->db->select('*');
		$this->db->from('login');
		$this->db->where_not_in('id', $id);
		$query = $this->db->get();

		$user['users']= $query->result();


		$this->load->view('add_transfer_fund',$user);
	}

	public function transferStore()
	{

		$amount=$this->input->post('amount');
		echo $amount.'<br>';
		$cblance=$this->fund_model->amountCheck($_SESSION['user_id']);

		$checkTpin = $this->user_model->checkTpin($this->input->post('t_pin'));

		$currentBalance= ((int)$cblance->c_balance * (int)$amount);

        $this->form_validation->set_rules('amount', 'amount', 'required');
        $this->form_validation->set_rules('t_pin', 't_pin', 'required');
        $this->form_validation->set_rules('receiver_id', 'Account ', 'required');

			if ($this->form_validation->run())
			{
				if($amount<=$cblance->c_balance && $checkTpin==1){
					$transferFund['sender_id'] = $_SESSION['user_id'];
					$transferFund['sender_name'] = $_SESSION['firstname'];
					$transferFund['amount'] = $amount;
					$transferFund['receiver_id'] = $this->input->post('receiver_id');
					$transferFund['tr_date'] = date('Y-m-d');
					$query = $this->fund_model->transferInsert($transferFund);
					$this->fund_model->blanceMinus($currentBalance,$_SESSION['user_id']);
					$this->fund_model->blanceAdd($transferFund['receiver_id'],$amount);
					$this->session->set_flashdata('success', 'Transfer fund Add Successfully');
					redirect("fund/transfer",'refresh');
				}
				else if($cblance->c_balance<=$amount || $checkTpin!=1)
				{
					$this->session->set_flashdata('errorMessage', 'Wrong Information!! Amount or Tpin is Incorrect.');
					return $this->addTransferFund();
				}

			}

			else {
				return $this->addTransferFund();

			}

	}


	public function withdrwaAdd()
	{
		$this->load->view("withdraw");
	}

	public function withdrawStore()
	{
		$this->form_validation->set_rules('amount', 'amount', 'required');
		$this->form_validation->set_rules('t_pin', 't_pin', 'required');
		$cblance=$this->fund_model->amountCheck($_SESSION['user_id']);

		$checkTpin = $this->user_model->checkTpin($this->input->post('t_pin'));
		if ($this->form_validation->run())
		{
			$amount=$this->input->post('amount');

			$adminCharge=floatval ($amount * 5/100);

			$amountWithdraw=($amount-$adminCharge);
			$currentBalance=floatval($cblance->c_balance-$adminCharge);


			if($checkTpin==1)
			{

				$widthdraw['withdraw_user_id']=$_SESSION['user_id'];
				$widthdraw['request_name']=$_SESSION['firstname'];
				$widthdraw['amount']=$amount;
				$widthdraw['amount_withdraw']=$amountWithdraw;
				$widthdraw['admin_charge']=$adminCharge;
				$widthdraw['created_at']=date('Y-m-d');

				$this->fund_model->blanceMinus($currentBalance,$_SESSION['user_id']);
				$this->fund_model->withdrawInsert($widthdraw);
				$this->session->set_flashdata('success', '<span><strong>Success!</strong>withdraw add successfully.</span>');
				redirect('withdraw/add','refresh');

			}
			else
			{
				$this->session->set_flashdata('errorMessage', '<span><strong>Warning!</strong> your tpin does not match.</span>');
				$this->load->view("withdraw");

			}

		}
		else
		{
			$this->load->view("withdraw");
		}



	}

	public function withdrawHistory()
	{
		$data['allhistory']=$this->fund_model->getAllHistory();

		$this->load->view('withdraw_history',$data);

	}

	public function singleWithdraw()
	{
		$userId = $_SESSION['user_id'];
		$singleHistory['singleHistory']=$this->fund_model->singleHistory($userId);
		$this->load->view('single_history',$singleHistory);

	}

	public function transferHistory()
	{
		$data['allhistory']=$this->fund_model->getTransferHistory();


		$this->load->view('transfer_history',$data);
	}

	public function withdrawStatus()
	{

		$withdrawId = $this->uri->segment(3);
		$widthdrawAmount = $this->uri->segment(4);
		$withdraw_user_id = $this->uri->segment(5);
		$cblance=$this->fund_model->amountCheck($withdraw_user_id);
		$currentBalance=floatval($cblance->c_balance-$widthdrawAmount);
		$this->fund_model->blanceMinus($currentBalance,$withdraw_user_id);

		$this->db->set('fund_trn_st',1); //value that used to update column
		$this->db->set('approved_at',date('Y-m-d')); //value that used to update column
		$this->db->where('withdraw_id', $withdrawId); //which row want to upgrade
		$this->db->update('withdraw_fund');  //table name
		 redirect('withdraw/history','refresh');

	}

	public function addPurchase()
	{
			 $productId= $this->uri->segment(2);
			 $productInfo['productInfo']=$this->Product_model->getProduct($productId);

			 $this->load->view('add_purchase',$productInfo);
	}

	public function purchaseStore()
	{
		$productId=$this->input->post('product_id');
		$productPrice=$this->input->post('price');

		$cblance=$this->fund_model->amountCheck($_SESSION['user_id']);

		if($productPrice<=$cblance->c_balance)
		{
			$data['request_name']=$_SESSION['username'];
			$data['product_id']=$productId;
			$data['product_name']=$this->input->post('product_name');
			$data['price']=$productPrice;
			$data['point']=$this->input->post('point');
			$data['date_time']=date('Y-m-d');
			$data['created_at']=date('Y-m-d h:i:a');
			$data['updated_at']=date('Y-m-d h:i:a');
			$currentBalance=floatval($cblance->c_balance-$productPrice);
			$this->fund_model->blanceMinus($currentBalance,$_SESSION['user_id']);
			$this->session->set_flashdata('success', '<span><strong>Success!</strong>Your Purchase Successfully Done .</span>');
			redirect('add_purchase/'.$productId.'refresh');
		}

		else
		{
			$this->session->set_flashdata('errorMessage', '<span><strong>Warning!</strong>You do not have enough balance .</span>');
			redirect('add_purchase/'.$productId.'refresh');
		}


	}

	public function allProducts()
	{
		$product['products']=$this->fund_model->getAllproducts();

		$this->load->view('productlist',$product);
	}

}
