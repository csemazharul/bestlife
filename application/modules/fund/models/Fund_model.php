<?php


class Fund_model extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function transferInsert($transfer)
	{
		$this->db->insert('transfer', $transfer);

	}
	public function getAllFund()
	{
		$query = $this->db->get('fund_add_req');
		return $query->result();
	}

	public function getAllHistory()
	{
	$query = $this->db->get('withdraw_fund');
	return $query->result();
	}

	public function getTransferHistory()
	{
		$query = $this->db->get('transfer');
		return $query->result();
	}


	public function amountCheck($id)
	{
		$this->db->select('c_balance');
		$this->db->from('login');
		$this->db->where('id',$id);
		$query=$this->db->get();
		return $query->first_row();

	}

	public function blanceMinus($currentBalance,$id)
	{
		$this->db->set('c_balance',$currentBalance); //value that used to update column
		$this->db->where('id', $id); //which row want to upgrade
		return $this->db->update('login');  //table name
	}

	public function blanceAdd($id,$newAmount)
	{
		$this->db->select('c_balance');
		$this->db->from('login');
		$this->db->where('id',$id);
		$query=$this->db->get();
		$receiverBalance= $query->first_row();
		$total=($receiverBalance->c_balance+$newAmount);
		$this->db->set('c_balance',$total); //value that used to update column
		$this->db->where('id', $id); //which row want to upgrade
		return $this->db->update('login');  //table name
	}

	public function withdrawInsert($data)
	{
		$this->db->insert('withdraw_fund', $data);
	}
	public function singleHistory($id)
	{
		$this->db->select('*');
		$this->db->from('withdraw_fund');
		$this->db->where('withdraw_user_id',$id);
		$query=$this->db->get();
		return $query->result();
	}

	public function getAllproducts()
	{
		$query = $this->db->get('products');
		return $query->result();
	}





}
