<?php


class subcategory extends MX_Controller
{
	public function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->load->model('category/Category_model');
		$this->load->model('Subcategory_model');
		$this->load->library('session');

	}

	public function subCreate()
	{

		$category['categories']=$this->Category_model->getAllCategories();


		$this->load->view('subcategory/create.php',$category);
	}

	public function subCategoryList()
	{

		$subCategory['subcategories']=$this->Subcategory_model->getAllSubCategories();

		$this->load->view('subcategory/list.php',$subCategory);
	}

	public function subStore()
	{

		$subCategory['title'] = $this->input->post('title');
		$subCategory['category_id'] = $this->input->post('category_id');

		$subCategory['created_at'] =date('Y-m-d h:i:a');
		$subCategory['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->Subcategory_model->subCategoryInsert($subCategory);

		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Sub Category Add Successfully.</span>');


		redirect('sub/list','refresh');

	}

	public function subCategoryEdit()
	{
		$id = $this->uri->segment(3);

		$category['subcategory'] = $this->Subcategory_model->getCategory($id);


		$this->load->view('subcategory/edit',$category);
	}

	public function subUpdate()
	{

		$id = $this->uri->segment(3);

		$categoryData= $this->Subcategory_model->getCategory($id);
		$category['title'] = $this->input->post('title');
		$category['created_at'] = $categoryData['created_at'];
		$category['updated_at'] =date('Y-m-d h:i:a');
		$query = $this->Subcategory_model->categoryUpdate($category,$id);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Sub Category Update Successfully.</span>');
		redirect('sub/list','refresh');

	}

	public function delete(){
		$id = $this->uri->segment(3);

		$query = $this->Subcategory_model->deleteCategory($id);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Sub Category Delete Successfully.</span>');
		redirect('sub/list','refresh');
	}

	public function findSubcategory()
	{
		$id = $this->uri->segment(2);
		$this->db->select('*');

		$this->db->from('categories');
		$this->db->where('categories.id',$id);
		$this->db->join('subcategories', 'categories.id=subcategories.category_id');

		$query = $this->db->get();

		echo json_encode($query->result());
	}

}
