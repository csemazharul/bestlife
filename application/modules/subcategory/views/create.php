<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header text-uppercase">Sub Category Create</div>
					<div class="card-body">

						<form method="post" action="<?php echo base_url()?>sub/store" enctype="multipart/form-data">



							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Select Your Category</label>
								<div class="col-sm-9">
									<select id="subcat_id" name="category_id" class="form-control">
										<option disabled selected>Choose Category</option>
										<?php
										foreach($categories as $category) {
											?>
											<option value="<?php echo $category->id?>"><?php echo $category->name?></option>
											<?php
										}
										?>
									</select>

								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">sub category name</label>
								<div class="col-sm-9">
									<input type="text" name="title" id="basic-input" class="form-control">
								</div>
							</div>



							<div class="form-footer">

								<button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Submit</button>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div><!--End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
