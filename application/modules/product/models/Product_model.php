<?php


class Product_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function productInsert($product)
	{

		$this->db->insert('products', $product);

	}
	public function getAllproducts()
	{
		$query = $this->db->get('products');
		return $query->result();
	}

	public function getProduct($id)
	{
		$query = $this->db->get_where('products',array('id'=>$id));
		return $query->row_array();
	}

	public function productUpdate($product, $id)
	{
		$this->db->where('products.id', $id);
		return $this->db->update('products', $product);
	}

	public function deleteProduct($id)
	{
		$this->db->where('products.id', $id);
		return $this->db->delete('products');
	}


}
