<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header text-uppercase">Product Create</div>
					<div class="card-body">

						<form method="post" action="<?php echo base_url("product/update/".$product['id'])?>" enctype="multipart/form-data">

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Title</label>
									<div class="col-sm-9">
										<input type="text" value="<?php echo $product['title']?>" name="title" id="basic-input" class="form-control">
									</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Select Your Category</label>
								<div class="col-sm-9">
									<select id="category_id" onchange="getSubcategory()" name="category_id" class="form-control">
										<option disabled selected>select your category</option>
										<?php
										foreach($categories as $category) {
											?>

											<option value="<?php echo $category->id?>" <?php if($product['category_id']==$category->id ) echo "selected"; ?>><?php echo $category->name?></option>
											<?php
										}
										?>
									</select>

								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Select Your Sub Category</label>
								<div class="col-sm-9">
									<select id="sub_id" name="sub_id" class="form-control">

									</select>

								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">Short Description</label>
								<div class="col-sm-9">
									<textarea rows="8" name="short_description" class="form-control" id="basic-textarea"><?php echo $product['short_description'] ?></textarea>
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Picture</label>
								<div class="col-sm-9">
									<input type="file" name="picture" class="form-control"><br>
									<img src="<?php echo base_url("upload/images/".$product['picture']) ?>" width="100px" height="80px" />
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">price</label>
								<div class="col-sm-9">
									<input type="number" value="<?php echo $product['price'] ?>" name="price" id="basic-input" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">point</label>
								<div class="col-sm-9">
									<input type="number" value="<?php echo $product['point'] ?>" name="point" id="basic-input" class="form-control">
								</div>
							</div>


							<div class="form-footer">

								<button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div><!--End Row-->
	</div>
	<!-- End container-fluid-->


</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
