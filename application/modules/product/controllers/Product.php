<?php


class Product extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Product_model');
		$this->load->model('category/Category_model');
		$this->load->library('session');

		if(!$_SESSION['user_email'])
		{
			redirect('login','refresh');
		}

	}

	public function productCreate()
	{
		$columns = '`id`,`name`';
		$this->db->select($columns, FALSE);
	   $query=$this->db->get('categories');
	   $category['categories']=$query->result();

		$this->load->view('product/create.php',$category);
	}

	public function productList()
	{
		$product['products']=$this->Product_model->getAllproducts();

		$this->load->view('product/list.php',$product);
	}



	public function productStore()
	{
		if(!empty($_FILES['picture']['name']))
		{
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['picture']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('picture'))
			{
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture = '';
			}
		}else{
			$picture = '';
		}

		$product['category_id'] = $this->input->post('category_id');
		$product['sub_id'] = $this->input->post('sub_id');
		$product['title'] = $this->input->post('title');
		$product['short_description'] = $this->input->post('short_description');
		$product['picture'] = $picture;
		$product['price'] = $this->input->post('price');
		$product['point'] = $this->input->post('point');
		$product['created_at'] =date('Y-m-d h:i:a');
		$product['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->Product_model->productInsert($product);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Product Add Successfully.</span>');

		redirect('product/list','refresh');

	}

	public function productEdit()
	{
		$columns = '`id`,`name`';
		$this->db->select($columns, FALSE);
		$query=$this->db->get('categories');
		$data['categories']=$query->result();
		$id = $this->uri->segment(3);
		$data['product'] = $this->Product_model->getProduct($id);
		$this->load->view('product/edit',$data);
	}

	public function productUpdate()
	{

		$id = $this->uri->segment(3);

		$productData= $this->Product_model->getProduct($id);


		if(!empty($_FILES['picture']['name'])){
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['picture']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('picture')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture ='';
			}
		}else{
			$picture =  $productData['picture'];
		}

		$product['category_id'] = $this->input->post('category_id');
		$product['sub_id'] = $this->input->post('sub_id');
		$product['title'] = $this->input->post('title');
		$product['short_description'] = $this->input->post('short_description');
		$product['picture'] = $picture;
		$product['price'] = $this->input->post('price');
		$product['point'] = $this->input->post('point');
		$product['created_at']=$productData['created_at'];
		$product['updated_at']=date('Y-m-d h:i:a');
		$query = $this->Product_model->productUpdate($product,$id);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Product Update Successfully.</span>');
		redirect('product/list','refresh');

	}

	public function delete()
	{
		$id = $this->uri->segment(3);

		$query = $this->Product_model->deleteProduct($id);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Product Delete Successfully.</span>');

		redirect('product/list','refresh');
	}





}
