<?php
class User_model extends CI_model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function register_user($user){
		$this->db->insert('user', $user);

	}

	public function login_user($email,$pass){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_email',$email);
		$this->db->where('user_password',$pass);
		$this->db->where('email_verified',1);
		$query=$this->db->get();
		if($query)
		{
		 	return $query->result_array();
		}
		else{
			return false;
		}
	}

	public function verified_check($emailVerified){

		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email_verified',$emailVerified);
		 $query=$this->db->get();

		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}

	}


}


?>
