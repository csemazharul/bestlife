<?php
$this->load->view('backend/users/layout/header');
?>
<!-- Start wrapper-->
<div id="wrapper">
	<div class="card card-authentication1 mx-auto my-5 animated zoomIn">
		<div class="card-body">
			<div class="alert alert-danger"  id="message">
				<?php echo $this->session->flashdata('message'); ?>
			</div>
			<div class="card-content p-2">
				<div class="text-center">

				</div>
				<div class="card-title text-uppercase text-center py-2">Sign In</div>
				<form method="post" action="<?php echo base_url()?>login/store">
					<div class="form-group">
						<div class="position-relative has-icon-left">
							<label for="exampleInputUsername" class="sr-only">Username</label>
							<input type="text" name="user_email" id="exampleInputUsername" class="form-control" placeholder="Username">
							<div class="form-control-position">
								<i class="icon-user"></i>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="position-relative has-icon-left">
							<label for="exampleInputPassword" class="sr-only">Password</label>
							<input type="password" name="user_password" id="exampleInputPassword" class="form-control" placeholder="Password">
							<div class="form-control-position">
								<i class="icon-lock"></i>
							</div>
						</div>
					</div>
					<div class="form-row mr-0 ml-0">
						<div class="form-group col-6">
							<div class="icheck-material-primary">
								<input type="checkbox" id="user-checkbox" checked="" />
								<label for="user-checkbox">Remember me</label>
							</div>
						</div>
						<div class="form-group col-6 text-right">
							<a href="authentication-reset-password.html">Reset Password</a>
						</div>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-danger shadow-danger btn-block waves-effect waves-light">Sign In</button>
					</div>
					<div class="form-group text-center">
						<p class="text-muted">Not a Member ? <a href="<?php echo base_url()?>register"> Sign Up here</a></p>
					</div>
					<div class="form-group text-center">
						<hr>
						<h5>OR</h5>
					</div>
					<div class="form-group text-center">
						<button type="button" class="btn btn-facebook shadow-facebook text-white btn-block waves-effect waves-light"><i class="fa fa-facebook-square"></i> Sign In With Facebook</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!--Start Back To Top Button-->
	<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
	<!--End Back To Top Button-->
</div><!--wrapper-->
<?php
$this->load->view('backend/users/layout/footer');
?>
