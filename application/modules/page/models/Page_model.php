<?php


class Page_model extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->load->database();

	}

	public function pageInsert($page){


		$this->db->insert('pages', $page);

	}
	public function getAllPages()
	{
		$query = $this->db->get('pages');
		return $query->result();
	}

	public function getPage($id)
	{
		$query = $this->db->get_where('pages',array('id'=>$id));
		return $query->row_array();
	}

	public function pageUpdate($page, $id){
		$this->db->where('pages.id', $id);
		return $this->db->update('pages', $page);
	}

	public function deletePage($id)
	{
		$this->db->where('pages.id', $id);
		return $this->db->delete('pages');
	}

	public function aboutData($menu)
	{
		$this->db->select("*");
		$this->db->from("pages");
		$this->db->limit(1);
		$this->db->where('menu_name',$menu);
		$this->db->order_by('id',"DESC");
		$query = $this->db->get();

		return $query->first_row();
	}

public function brandImg($menu)
{
	$this->db->select("*");
	$this->db->from("pages");
	$this->db->where('menu_name',$menu);
	$this->db->order_by('id',"DESC");
	$query = $this->db->get();

	return $query->result();
}
	public function allData($menu)
	{
		$this->db->select("*");
		$this->db->from("pages");
		$this->db->where('menu_name',$menu);
		$this->db->order_by('id',"DESC");
		$query = $this->db->get();

		return $query->result();
	}

	public function teamInfo($menu)
	{
		$this->db->select("*");
		$this->db->from("pages");
		$this->db->limit(4);
		$this->db->where('menu_name',$menu);
		$this->db->order_by('id',"DESC");
		$query = $this->db->get();

		return $query->result();
	}

	public function gallery($menu)
	{
		$this->db->select('photo');
		$this->db->from("pages");
		$this->db->limit(9);
		$this->db->where('menu_name',$menu);
		$this->db->order_by('id',"DESC");
		$query = $this->db->get();

		return $query->result();
	}

	public function getRows()
	{
		$this->db->select('*');
		$this->db->from('products');
		$query = $this->db->get();
		$result = ($query->num_rows() > 0)?$query->result_array():FALSE;



		// Return fetched data
		return $result;
	}

	public function subProduct($id)
	{
		$this->db->select("*");
		$this->db->from("products");
		$this->db->where('sub_id',$id);
		$this->db->order_by('id',"DESC");
		$query = $this->db->get();

		return $query->result();
	}





}
