<?php


class Page extends MX_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Page_model');
		$this->load->model('product/Product_model');
		$this->load->model('slider/Slider_model');
		$this->load->model('category/Category_model');
		$this->load->library('session');

		// Load pagination library
		$this->load->library('pagination');

		// Per page limit
		$this->perPage = 5;

	}

	public function home()
	{
		$data['sliders']=$this->Slider_model->getAllSliders();
		$data['about'] =$this->Page_model->aboutData('About');
		$data['vission'] =$this->Page_model->aboutData('Vission');
		$data['mission'] =$this->Page_model->aboutData('Mission');
		$data['brands'] =$this->Page_model->brandImg('Brand');


		$this->load->view('/index',$data);
	}

	public function about()
	{
		$menu = $this->uri->segment(2);
		$data['about'] =$this->Page_model->aboutData($menu);
		$this->load->view('/about',$data);
	}

	public function vission()
	{
		$menu = $this->uri->segment(2);

		$data['vission'] =$this->Page_model->aboutData('vission');

		$this->load->view('/vission',$data);
	}

	public function mission()
	{
		$menu = $this->uri->segment(2);
		$data['mission'] =$this->Page_model->aboutData($menu);
		$this->load->view('/mission',$data);
	}



	public function services()
	{
		$this->load->view('/services');
	}


	public function contact()
	{
		$this->load->view('/contact');

	}

	public function team()
	{
		$menu = $this->uri->segment(2);
		$data['alldata'] =$this->Page_model->teamInfo($menu);

		$this->load->view('/team',$data);

	}

	public function gallery()
	{
		$menu = $this->uri->segment(2);

		$data['alldata'] =$this->Page_model->gallery($menu);

		$this->load->view('/gallery',$data);

	}
	public function news()
	{
		$menu = $this->uri->segment(2);
		$data['alldata'] =$this->Page_model->allData($menu);

		$this->load->view('/news',$data);
	}

	public function singleNews()
	{
		$id = $this->uri->segment(2);
		$query = $this->db->get_where('pages',array('id'=>$id));
		$data['singlenews']= $query->first_row();

		$this->load->view('/single_news',$data);

	}


	public function products()
	{
		$data['categories']=$this->Category_model->getAllCategories();
		$this->load->library('pagination');
		$data['categories']=$this->Category_model->getAllCategories();
		 $query2=$this->db->get('products');
	    $config = array();
	    $config["base_url"] = base_url()."page/page/products/";
	    $config["total_rows"] = $query2->num_rows();
	    $config["per_page"] = 2;
	    $config["uri_segment"] = 3;
	    $config['full_tag_open'] = '<ul class="pagination">';
	    $config['full_tag_close'] = '</ul>';
	    $config['prev_link'] = '&laquo;';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['next_link'] = '&raquo;';


    	 $this->pagination->initialize($config);
    
    	 $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
    	 $q = $this->db->limit($config["per_page"], $page)->get('products');
    	 $data['products']= $q->result();
    	 $data["links"] = $this->pagination->create_links();
    
    	 $this->load->view('/our_product',$data);

	}


	public function admin()
	{
		if(isset($_SESSION['user_email']))
		{
			$this->load->view('backend/index');

		}
		else
		{
			redirect('/login');

		}
	}

	public function pageCreate()
	{
		$this->load->view('page/page/create.php');
	}
	public function pageList()
	{
		$page['pages']=$this->Page_model->getAllPages();

		$this->load->view('page/page/list.php',$page);
	}

	public function pageStore()
	{
		if(!empty($_FILES['photo']['name'])){
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['photo']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('photo')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture = '';
			}
		}else{
			$picture = '';
		}


		$page['title'] = $this->input->post('title');
		$page['menu_name'] = $this->input->post('menu_name');
		$page['short_description'] = $this->input->post('short_description');
		$page['long_description'] = $this->input->post('long_description');
		$page['photo'] = $picture;
		$page['created_by'] =$_SESSION['user_name'];
		$page['created_at'] =date('Y-m-d h:i:a');
		$page['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->Page_model->pageInsert($page);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Page Add Successfully.</span>');

		redirect('page/create','refresh');

	}

	public function pageEdit()
	{
		$id = $this->uri->segment(3);

		$page['page'] = $this->Page_model->getPage($id);

		$this->load->view('page/page/edit',$page);
	}

	public function pageUpdate()
	{

		$id = $this->uri->segment(3);
		$page= $this->Page_model->getPage($id);
		$create_at=$page['created_at'];

		if(!empty($_FILES['photo']['name'])){
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['photo']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('photo')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture ='';
			}
		}else{
			$picture =  $page['photo'];
		}

		$page['title'] = $this->input->post('title');
		$page['menu_name'] = $this->input->post('menu_name');
		$page['short_description'] = $this->input->post('short_description');
		$page['long_description'] = $this->input->post('long_description');
		$page['photo'] = $picture;
		$page['created_by'] =$_SESSION['user_name'];
		$page['created_at'] =date('Y-m-d h:i:a');
		$page['updated_at'] =date('Y-m-d h:i:a');
		$query = $this->Page_model->pageUpdate($page,$id);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Page Update Successfully.</span>');

		redirect('page/list','refresh');

	}

	public function delete(){
		$id = $this->uri->segment(3);

		$query = $this->Page_model->deletePage($id);
		$this->session->set_flashdata('success', '<span><strong>Success!</strong>Page Delete Successfully.</span>');

		redirect('page/list','refresh');
	}

	public function subProduct()
	{
		$id = $this->uri->segment(2);
		$data['categories']=$this->Category_model->getAllCategories();
		$data['products'] = $this->Page_model->subProduct($id);

		$this->load->view('sub_category_product',$data);

	}


}
