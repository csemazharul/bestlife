<?php
$this->load->view('frontend/layout/header');
?>

<!--Breadcrumb-->
<div class="breadcrumb_wrapper">

		<div class="breadcrumb_block">
				<ul>
						<li><a href="index.html">home</a></li>
						<li><a href="aboutus.html">aboutus</a></li>
				</ul>
		</div>
</div>
<!--About Section-->
<div class="clv_about_wrapper clv_section">
		<div class="container">
				<div class="row">
						<div class="col-md-6">
								<div class="about_img">
										<img src="<?php echo base_url('upload/images/'.$about->photo)?>" alt="image" style="width: 100%;
										height: 350px;" />
								</div>
						</div>
						<div class="col-md-6">
								<div class="about_content">
										<div class="about_heading">
												<h2><?php echo $about->title;?></h2>
												<h6><?php echo $about->short_description;?></h6>

										</div>
										<p><?php echo $about->long_description;?></p>


								</div>
						</div>
				</div>
		</div>
</div>
<!--Testimonial-->
	<div class="clv_testimonial_wrapper clv_section">
		<div class="container">
				<div class="row justify-content-center">
						<div class="col-lg-6 col-md-6">
								<div class="clv_heading white_heading">
										<h3>what people say about us</h3>
										<div class="clv_underline"><img src="<?php echo base_url('ui/frontend/images/')?>/underline2.png" alt="image" /></div>
										<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
								</div>
						</div>
				</div>
				<div class="row">
						<div class="col-lg-12 col-md-12">
								<div class="testimonial_slider">
										<div class="swiper-container">
												<div class="swiper-wrapper">
														<div class="swiper-slide">
																<div class="testimonial_slide">
																		<span class="rounded_quote"><img src="<?php echo base_url('ui/frontend/images/')?>/quote.png" alt="image" /></span>
																		<span class="bg_quote"><img src="<?php echo base_url('ui/frontend/images/')?>/bg_quote.png" alt="image" /></span>
																		<div class="client_img">
																				<img src="<?php echo base_url('ui/frontend/images/')?>/client.jpg" alt="image" />
																		</div>
																		<div class="client_message">
																				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ilabore et dadhjiolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi liquip ex ea commodoersio consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugt nulla pariatuaerniri Excepteur sint occaecat cupidatat non proident.</p>
																				<h3>Halil Alex <span> Agriculture Expert</span></h3>
																		</div>
																</div>
														</div>
														<div class="swiper-slide">
																<div class="testimonial_slide">
																		<span class="rounded_quote"><img src="<?php echo base_url('ui/frontend/images/')?>/quote.png" alt="image" /></span>
																		<span class="bg_quote"><img src="<?php echo base_url('ui/frontend/images/')?>/bg_quote.png" alt="image" /></span>
																		<div class="client_img">
																				<img src="<?php echo base_url('ui/frontend/images/')?>/client2.jpg" alt="image" />
																		</div>
																		<div class="client_message">
																				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi liquip ex ea commodoersio consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugt nulla pariatuaerniri Excepteur sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ilabore et dadhjiolore magna aliqua.</p>
																				<h3>john paradox <span> Agriculture Expert</span></h3>
																		</div>
																</div>
														</div>
												</div>
												<!-- Add Arrows -->
												<span class="slider_arrow testimonial_left left_arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></span>
												<span class="slider_arrow testimonial_right right_arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>

<!--Partners-->
<div class="#">
		<div class="container">

				<div class="clv_newsletter_wrapper" style="top: -34px;">
						<div class="newsletter_text">
								<h2>get update from <br/>anywhere</h2>
								<h4>subscribe us to get more info</h4>
						</div>
						<div class="newsletter_field">
								<h3>don't miss out on the good news!</h3>
								<div class="newsletter_field_block">
										<input type="text" placeholder="Enter Your Email Here" />
										<a href="javascript:;">subscribe now</a>
								</div>
						</div>
				</div>
		</div>
</div>

<?php
$this->load->view('frontend/layout/footer');
?>
