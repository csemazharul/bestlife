<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<?php if(isset($_SESSION['success']))
				{
					?>
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<div class="alert-icon">
							<i class="icon-check"></i>
						</div>
						<div class="alert-message">
							<?php echo $this->session->flashdata('success'); ?>
						</div>
					</div>
					<?php
				}
				?>
				<div class="card">
					<div class="card-header text-uppercase">Page Create</div>
					<div class="card-body">

						<form method="post" action="<?php echo base_url('page/update/'.$page['id'])?>" enctype="multipart/form-data">
							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Page Name</label>
								<div class="col-sm-9">
									<select class="form-control"  name="menu_name">
										<option>--Select One-</option>
										<option value="About"<?php if($page['menu_name']=='About') echo 'selected';?>>About Us</option>
										<option value="Managment"<?php if($page['menu_name']=='Managment') echo 'selected';?>>Managment</option>
										<option value="Gallery"<?php if($page['menu_name']=='Gallery') echo 'selected';?>>Gallery</option>
										<option value="News"<?php if($page['menu_name']=='News') echo 'selected';?>>News</option>
										<option value="Brand"<?php if($page['menu_name']=='Brand"') echo 'selected';?>>Our Brand</option>
										<option value="Vission"<?php if($page['menu_name']=='Vission') echo 'selected';?>>Our Vision</option>
										<option value="Mission"<?php if($page['menu_name']=='Mission') echo 'selected';?>>Our Mission</option>
									</select>


								</div>
							</div>
							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Title</label>
								<div class="col-sm-9">
									<input type="text"  value="<?php echo $page['title'] ?>" name="title" id="basic-input" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Short Description</label>
								<div class="col-sm-9">
									<textarea rows="4" name="short_description" class="form-control" id="basic-textarea"><?php echo $page['short_description'] ?></textarea>
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">Long Description</label>
								<div class="col-sm-9">
									<textarea rows="8" name="long_description" class="form-control" id="basic-textarea"><?php echo $page['long_description'] ?></textarea>
								</div>
							</div>


							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Picture</label>
								<div class="col-sm-9">
									<input type="file" name="photo" id="basic-input" class="form-control"><br>
									<img src="<?php echo base_url('upload/images/'.$page['photo'])?>" width="100px" height="80px"/>
								</div>


							</div>

							<div class="form-footer">

								<button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div><!--End Row-->
	</div>
	<!-- End container-fluid-->

</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
