<?php
$this->load->view('frontend/layout/header');
?>
    <!--Breadcrumb-->
    <div class="breadcrumb_wrapper">

        <div class="breadcrumb_block">
            <ul>
                <li><a href="index.html">home</a></li>
                <li><a href="news.html">Single News</a></li>
            </ul>
        </div>
    </div>

	<div class="blog_sidebar_wrapper clv_section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="blog_left_section">
							<div class="blog_section">
								<div class="agri_blog_image">
									<img src="<?php echo base_url('upload/images/'.$singlenews->photo) ?>" alt="image">
									<span class="agri_blog_date">jan 06, 2019</span>
								</div>
								<div class="agri_blog_content">
									<h3><a href="blog_single.html"><?php echo $singlenews->title ?></a></h3>
									<div class="blog_user">
										<div class="user_name">
										
											<a href="javascript:;"><span><?php echo $singlenews->created_by ?></span></a>
										</div>

									</div>
                                    <p><?php echo $singlenews->short_description?></p><br>

                                    <p><?php echo $singlenews->long_description?></p>

								</div>
							</div>


						</div>
					</div>

				</div>
			</div>
		</div>



</div>
<?php
$this->load->view('frontend/layout/footer');
?>
