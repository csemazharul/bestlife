<?php
$this->load->view('frontend/layout/header');
?>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">

		<div class="breadcrumb_block">
				<ul>
						<li><a href="index.html">home</a></li>
						<li><a href="products.html">products</a></li>
				</ul>
		</div>
</div>

<!--Produst List-->
<div class="products_wrapper clv_section">
				<div class="container">
						<div class="row">
								<div class="col-lg-3 col-md-3">
										<div class="product_sidebar">
												<div class="product_block">
														<div class="sidebar_heading">
																<h3>search</h3>
																<img src="<?php echo base_url('ui/frontend/images/')?>/footer_underline.png" alt="image">
														</div>
														<div class="sidebar_search">
															<form action="<?php base_url('pagination')?>" method="post" id="myForm">
																<input type="text" name="searchKeyword" placeholder="Search here">
																<a href="javascript:;" onclick="myFunction()"  id="myForm"><i class="fa fa-search" aria-hidden="true"></i></a>
															</form>

														</div>
												</div>
												<div class="product_block">
														<div class="sidebar_heading">
																<h3>product categories</h3>
																<img src="<?php echo base_url('ui/frontend/images/')?>/footer_underline.png" alt="image">
														</div>
														<div class="product_category">
																<ul>

																	<?php foreach ($categories as $category) {
																		?>
																		<li>
																			<input  type="checkbox"  checked>
																			<p  id="<?php echo $category->id?>" onclick="findSubcategory(this)"><?php echo $category->name?><span></span></p>
																			<div class="product_category" id="sub_<?php echo $category->id?>" style="margin-left:20px">
																			</div>
																		</li>
																		<?php
																	}
																	?>
																</ul>
														</div>
												</div>

										</div>
								</div>
								<div class="col-lg-9 col-md-9">

										<div class="product_list_section">

												<div class="product_items_section">
														<ul>
															<?php foreach ($products as $product) {
																?>
																<li>
																	<div class="product_item_block">
																		<div class="org_product_block">
																			<div class="org_product_image"><img
																					src="<?php echo base_url('upload/images/'.$product->picture) ?>"
																					alt="image" width="253px" height="170px"></div>
																			<h4><?php echo $product->title?></h4>
																			<h3><span><i class="fa fa-usd"
																						 aria-hidden="true"></i></span><?php echo $product->price?>
																			</h3>
																			<a href="javascript:;">point <?php echo $product->point ?></a>
																		</div>
																		<div class="content_block">
																			<div class="product_price_box">
																				<h3>farm sprayer</h3>
																				<h5><span><i class="fa fa-usd"
																							 aria-hidden="true"></i></span>25
																				</h5>
																			</div>
																			<p>Farm & Garden</p>
																			<div class="rating_section">
																				<span>4.1</span>
																				<ul>
																					<li><a class="active"
																						   href="javascript:;"><i
																								class="fa fa-star"
																								aria-hidden="true"></i></a>
																					</li>
																					<li><a class="active"
																						   href="javascript:;"><i
																								class="fa fa-star"
																								aria-hidden="true"></i></a>
																					</li>
																					<li><a class="active"
																						   href="javascript:;"><i
																								class="fa fa-star"
																								aria-hidden="true"></i></a>
																					</li>
																					<li><a href="javascript:;"><i
																								class="fa fa-star"
																								aria-hidden="true"></i></a>
																					</li>
																					<li><a href="javascript:;"><i
																								class="fa fa-star"
																								aria-hidden="true"></i></a>
																					</li>
																				</ul>
																				<p>151 reviews</p>
																			</div>
																			<ul class="product_code">
																				<li>
																					<p>product code: 12948</p>
																				</li>
																				<li>
																					<p>availability:
																						<span>in stock</span></p>
																				</li>
																			</ul>
																			<p>Consectetur adipisicing elit sed do
																				eiusmod tempor incididunt utte labore et
																				dolore magna aliqua Ut enim ad minim</p>
																		</div>
																	</div>
																</li>
																<?php
															}
															?>
														</ul>
												</div>
												<div class="blog_pagination_section">
													<?php echo $this->pagination->create_links()?>
														<!-- <ul>

																<li class="blog_page_arrow">
																		<a href="javascript:;">
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="15px">
																				<path fill-rule="evenodd" fill="rgb(112, 112, 112)" d="M0.324,8.222 L7.117,14.685 C7.549,15.097 8.249,15.097 8.681,14.685 C9.113,14.273 9.113,13.608 8.681,13.197 L2.670,7.478 L8.681,1.760 C9.113,1.348 9.113,0.682 8.681,0.270 C8.249,-0.139 7.548,-0.139 7.116,0.270 L0.323,6.735 C0.107,6.940 -0.000,7.209 -0.000,7.478 C-0.000,7.747 0.108,8.017 0.324,8.222 Z"></path>
																				</svg>
																				<span>prev</span>
																		</a>
																</li>
																<li><a href="javascript:;">01</a></li>
																<li><a href="javascript:;">02</a></li>
																<li><a href="javascript:;">....</a></li>
																<li><a href="javascript:;">12</a></li>
																<li><a href="javascript:;">13</a></li>
																<li class="blog_page_arrow">
																		<a href="javascript:;">
																				<span>next</span>
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19px" height="25px">
																				<path fill-rule="evenodd" fill="rgb(112, 112, 112)" d="M13.676,13.222 L6.883,19.685 C6.451,20.097 5.751,20.097 5.319,19.685 C4.887,19.273 4.887,18.608 5.319,18.197 L11.329,12.478 L5.319,6.760 C4.887,6.348 4.887,5.682 5.319,5.270 C5.751,4.861 6.451,4.861 6.884,5.270 L13.676,11.735 C13.892,11.940 14.000,12.209 14.000,12.478 C14.000,12.747 13.892,13.017 13.676,13.222 Z"></path>
																				</svg>
																		</a>
																</li>
														</ul> -->
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="#">
						<div class="container">

								<div class="clv_newsletter_wrapper" style="top: -34px;">
										<div class="newsletter_text">
												<h2>get update from <br/>anywhere</h2>
												<h4>subscribe us to get more info</h4>
										</div>
										<div class="newsletter_field">
												<h3>don't miss out on the good news!</h3>
												<div class="newsletter_field_block">
														<input type="text" placeholder="Enter Your Email Here" />
														<a href="javascript:;">subscribe now</a>
												</div>
										</div>
								</div>
						</div>
				</div>
<!--Footer-->



<script>
    function myFunction() {

        document.getElementById("myForm").submit();
    }
</script>

<?php
$this->load->view('frontend/layout/footer');
?>
