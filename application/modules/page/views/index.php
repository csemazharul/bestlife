<?php
$this->load->view('frontend/layout/header');
?>

<!--Banner Slider-->
<div class="clv_banner_slider">
		<!-- Swiper -->
		<div class="swiper-container">
				<div class="swiper-wrapper">

											<?php

											 foreach ($sliders as $slider)
												{
												 ?>
												 <div class="swiper-slide">
												 <div class="clv_slide slider" style="background:url(<?php echo base_url('upload/images/'.$slider->picture)?>) no-repeat center;">
				 										<div class="container">
												 <div class="clv_slide_inner">
														 <h2><?php echo $slider->title; ?></h2>
														 <p>
															 <?php echo $slider->short_description;?>
														 </p>
														 <div class="banner_btn">
																 <span class="left_line"></span>
																 <a href="javascript:;" class="clv_btn">discover more</a>
																 <span class="right_line"></span>
														 </div>
												 </div>
											 </div>
									 </div>
									 	</div>
											<?php } ?>



				</div>
				<!-- Add Arrows -->
				<span class="slider_arrow banner_left left_arrow">
<svg
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 width="10px" height="20px">
<path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
 d="M0.272,10.703 L8.434,19.703 C8.792,20.095 9.372,20.095 9.731,19.703 C10.089,19.308 10.089,18.668 9.731,18.274 L2.217,9.990 L9.730,1.706 C10.089,1.310 10.089,0.672 9.730,0.277 C9.372,-0.118 8.791,-0.118 8.433,0.277 L0.271,9.274 C-0.082,9.666 -0.082,10.315 0.272,10.703 Z"/>
</svg>
</span>
				<span class="slider_arrow banner_right right_arrow">
<svg
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 width="10px" height="20px">
<path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
 d="M9.728,10.703 L1.566,19.703 C1.208,20.095 0.627,20.095 0.268,19.703 C-0.090,19.308 -0.090,18.668 0.268,18.274 L7.783,9.990 L0.269,1.706 C-0.089,1.310 -0.089,0.672 0.269,0.277 C0.627,-0.118 1.209,-0.118 1.567,0.277 L9.729,9.274 C10.081,9.666 10.081,10.315 9.728,10.703 Z"/>
</svg>
</span>
		</div>
</div>

<!--Service About-->
<div class="clv_service_wrapper clv_section">
		<div class="container">
				<div class="row justify-content-center">

				</div>
				<div class="service_main_wrapper">
						<div class="row">
								<div class="col-lg-12 col-md-12">
										<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="service_block">
																<span></span>
																<div class="service_icon">
																		<img src="<?php echo base_url('ui/frontend/images/')?>/new01.jpeg" alt="new-image" / style="width:100%;">
																</div>
																<h5>lorem lorem</h5>
														</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="service_block">
																<span></span>
																<div class="service_icon">
																		<img src="<?php echo base_url('ui/frontend/images/')?>/new02.jpeg" alt="new-image" / style="width:100%;">
																</div>
																<h5>lorem lorem</h5>
														</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="service_block">
																<span></span>
																<div class="service_icon">
																		<img src="<?php echo base_url('ui/frontend/images/')?>/new03.jpeg" alt="new-image" / style="width:100%;">
																</div>
																<h5>lorem lorem</h5>
														</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
														<div class="service_block">
																<span></span>
																<div class="service_icon">
																		<img src="<?php echo base_url('ui/frontend/images/')?>/new04.jpeg" alt="new-image" / style="width:93%;">
																</div>
																<h5>lorem lorem</h5>
														</div>
												</div>
										</div>
								</div>

						</div>
				</div>
		</div>
</div>
<!--Testimonial-->
<!--Blog-->
<div class="garden_blog_wrapper clv_section">
		<div class="container">
				<div class="row justify-content-center">
						<div class="col-lg-6 col-md-6">
								<div class="clv_heading white_heading">
										<h3>About Us</h3>
										<p>Dolor sit amet, consectetur adipisicing elit impor incididunt ....</p>
										<div class="clv_underline"><img src="<?php echo base_url('ui/frontend/images/')?>/underline2.png" alt="image" />
										</div>

								</div>
						</div>
				</div>
				<br>
				<div class="clv_features_wrapper ">
						<div class="container">
								<div class="row">
										<div class="col-md-4 col-lg-4">
												<div class="feature_block">
														<div class="feature_img">
																<img src="<?php echo base_url('upload/images/'.$about->photo)?>" alt="image" style="width: 160px;" />
														</div>
														<h3>About Company</h3>
														<p><?php echo $about->short_description?></p>
														<a href="<?php echo base_url('about/About') ?>" class="clv_btn">read more</a>
												</div>
										</div>
										<div class="col-md-4 col-lg-4">
												<div class="feature_block">
														<div class="feature_img">
																<img src="<?php echo base_url('upload/images/'.$vission->photo)?>" alt="image" style="width: 160px;" />
														</div>
														<h3>Our Vision</h3>
														<p><?php echo $vission->short_description?></p>
														<a href="<?php echo base_url('vission/vission') ?>" class="clv_btn">read more</a>
												</div>
										</div>
										<div class="col-md-4 col-lg-4">
												<div class="feature_block">
														<div class="feature_img">
																<img src="<?php echo base_url('upload/images/'.$mission->photo)?>" alt="image" style="width: 160px;" />
														</div>
														<h3>Our Mission</h3>
														<p><?php echo $mission->short_description?></p>
														<a href="<?php echo base_url('mission/mission') ?>" class="clv_btn">read more</a>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>
<!--Service 2-->


<!--Partner-->
<div class="clv_partner_wrapper clv_section">

		<div class="container">

				<div class="row">
						<div class="col-lg-12 col-md-12">
								<div class="clv_heading">
										<h3>our brand</h3>
										<div class="clv_underline"><img src="<?php echo base_url('ui/frontend/images/agri_underline2.png')?>" alt="image" /></div>
										<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
								</div>
						</div>
						<div class="col-md-12">

								<div class="partner_slider">
										<div class="swiper-container">
												<div class="swiper-wrapper">
													<?php
														foreach ($brands as $brand) {

													?>
																				<div class="swiper-slide">
																						<div class="partner_slide">
																								<div class="partner_image">
																										<span>
																											<img src="<?php echo base_url('upload/images/'.$brand->photo)?>" width="120px" height="100px">
																										</span>
																								</div>
																						</div>
																				</div>
																				<?php
																			}
																				?>



																		</div>
																</div>
														</div>
												</div>
										</div>
										<!-- Add Arrows -->
										<span class="slider_arrow partner_left left_arrow">
			<svg
			 xmlns="http://www.w3.org/2000/svg"
			 xmlns:xlink="http://www.w3.org/1999/xlink"
			 width="10px" height="15px">
			<path fill-rule="evenodd"  fill="rgb(226, 226, 226)"
			 d="M0.324,8.222 L7.117,14.685 C7.549,15.097 8.249,15.097 8.681,14.685 C9.113,14.273 9.113,13.608 8.681,13.197 L2.670,7.478 L8.681,1.760 C9.113,1.348 9.113,0.682 8.681,0.270 C8.249,-0.139 7.548,-0.139 7.116,0.270 L0.323,6.735 C0.107,6.940 -0.000,7.209 -0.000,7.478 C-0.000,7.747 0.108,8.017 0.324,8.222 Z"/>
			</svg>
		</span>
										<span class="slider_arrow partner_right right_arrow">
			<svg
			 xmlns="http://www.w3.org/2000/svg"
			 xmlns:xlink="http://www.w3.org/1999/xlink"
			 width="19px" height="25px">
			<path fill-rule="evenodd" fill="rgb(226, 226, 226)"
			 d="M13.676,13.222 L6.883,19.685 C6.451,20.097 5.751,20.097 5.319,19.685 C4.887,19.273 4.887,18.608 5.319,18.197 L11.329,12.478 L5.319,6.760 C4.887,6.348 4.887,5.682 5.319,5.270 C5.751,4.861 6.451,4.861 6.884,5.270 L13.676,11.735 C13.892,11.940 14.000,12.209 14.000,12.478 C14.000,12.747 13.892,13.017 13.676,13.222 Z"/>
			</svg>
		</span>
								</div>
						</div>
				</div>

				<!--Newsletter-->
				<div class="clv_newsletter_wrapper">
						<div class="newsletter_text">
								<h2>get update from <br/>anywhere</h2>
								<h4>subscribe us to get more info</h4>
						</div>
						<div class="newsletter_field">
								<h3>don't miss out on the good news!</h3>
								<div class="newsletter_field_block">
										<input type="text" placeholder="Enter Your Email Here" />
										<a href="javascript:;">subscribe now</a>
								</div>
						</div>
				</div>
		</div>
</div>

<?php
$this->load->view('frontend/layout/footer');
?>
