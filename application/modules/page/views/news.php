<?php
$this->load->view('frontend/layout/header');
?>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">

    <div class="breadcrumb_block">
        <ul>
            <li><a href="index.html">home</a></li>
            <li><a href="news.html">news</a></li>
        </ul>
    </div>
</div>

<!--news sections-->
<div class="clv_blog_wrapper clv_section">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6 col-md-6">
        <div class="clv_heading">
          <h3>News </h3>
          <div class="clv_underline"><img src="<?php echo base_url('ui/frontend/images/')?>/underline3.png" alt="image" /></div>

        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="blog_slider">
          <div class="swiper-container">
            <div class="swiper-wrapper">
				<?php
				foreach ($alldata as $data) {
					?>
					<div class="swiper-slide">
						<div class="blog_slide">
							<div class="blog_image">
								<img src="<?php echo base_url('upload/images/'.$data->photo) ?>" width="370px" height="270px" alt="image"
									 class="img-fluid"/>
							</div>
							<div class="blog_content">
								<h6 class="blog_date">jan 06, 2019</h6>
								<h4 class="blog_title"><?php echo $data->title?></h4>
								<div class="blog_user">
									<div class="user_name">
										<img src="<?php echo base_url('ui/frontend/images/') ?>/user.png" alt="image"/>
										<a href="javascript:;"><span><?php echo $data->created_by?></span></a>
									</div>

								</div>
								<p><?php echo $data->short_description ?></p>
								<a href="<?php echo base_url('single_news/'.$data->id)?>">read more <span>
                                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
								</a>
							</div>
						</div>
					</div>
					<?php
				}
				?>
            </div>
          </div>
          <!-- Add Arrows -->
          <span class="slider_arrow blog_left left_arrow">
            <svg
             xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink"
             width="10px" height="20px">
            <path fill-rule="evenodd"  fill="rgb(226, 226, 226)"
             d="M0.272,10.703 L8.434,19.703 C8.792,20.095 9.372,20.095 9.731,19.703 C10.089,19.308 10.089,18.668 9.731,18.274 L2.217,9.990 L9.730,1.706 C10.089,1.310 10.089,0.672 9.730,0.277 C9.372,-0.118 8.791,-0.118 8.433,0.277 L0.271,9.274 C-0.082,9.666 -0.082,10.315 0.272,10.703 Z"/>
            </svg>
          </span>
          <span class="slider_arrow blog_right right_arrow">
            <svg
             xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink"
             width="10px" height="20px">
            <path fill-rule="evenodd"  fill="rgb(226, 226, 226)"
             d="M9.728,10.703 L1.566,19.703 C1.208,20.095 0.627,20.095 0.268,19.703 C-0.090,19.308 -0.090,18.668 0.268,18.274 L7.783,9.990 L0.269,1.706 C-0.089,1.310 -0.089,0.672 0.269,0.277 C0.627,-0.118 1.209,-0.118 1.567,0.277 L9.729,9.274 C10.081,9.666 10.081,10.315 9.728,10.703 Z"/>
            </svg>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Partners-->
<div class="#">
    <div class="container">

        <div class="clv_newsletter_wrapper" style="top: -34px;">
            <div class="newsletter_text">
                <h2>get update from <br/>anywhere</h2>
                <h4>subscribe us to get more info</h4>
            </div>
            <div class="newsletter_field">
                <h3>don't miss out on the good news!</h3>
                <div class="newsletter_field_block">
                    <input type="text" placeholder="Enter Your Email Here" />
                    <a href="javascript:;">subscribe now</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->load->view('frontend/layout/footer');
?>
