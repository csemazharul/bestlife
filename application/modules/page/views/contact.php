<?php
$this->load->view('frontend/layout/header');
?>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">

		<div class="breadcrumb_block">
				<ul>
						<li><a href="index.html">home</a></li>
						<li>contact us</li>
				</ul>
		</div>
</div>

<!--Contact Form-->
<div class="contact_form_wrapper clv_section">
		<div class="container">
				<div class="row">
						<div class="col-lg-7 col-md-7">
								<div class="contact_form_section">
										<div class="row">
												<div class="col-md-12 col-lg-12">
														<h3>send message us</h3>
												</div>
					<form>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<input type="text" name="first_name" class="form_field require" placeholder="First Name" >
														</div>
												</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<input type="text" name="last_name" class="form_field require" placeholder="Last Name" >
														</div>
												</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<input type="text" name="email" class="form_field require" placeholder="Email" data-valid="email" data-error="Email should be valid." >
														</div>
												</div>
												<div class="col-md-6 col-lg-6">
														<div class="form_block">
																<input type="text" name="subject" class="form_field require" placeholder="Subject" >
														</div>
												</div>
												<div class="col-md-12 col-lg-12">
														<div class="form_block">
																<textarea placeholder="Message" name="message" class="form_field require" ></textarea>
							<div class="response"></div>
														</div>
												</div>
												<div class="col-md-12 col-lg-12">
														<div class="form_block">
																<button type="button" class="clv_btn submitForm" data-type="contact">send</button>
														</div>
												</div>
					</form>
										</div>
								</div>
						</div>
						<div class="col-lg-5 col-md-5">
								<div class="working_time_section">
										<div class="timetable_block">
												<h5>GET IN TOUCH WITH US</h5>
												<ul>
														<li>
																<p>Email</p>
																<p>BestlifeWorld@gmail.com</p>
														</li>
														<li>
																<p>Address</p>
																<p>8654 Bellevue Drive

																				Rock Hill, SC 29730</p>
														</li>
														<li>
																<p>Contact Us</p>
																<p>48572489589</p>

														</li>
														<li>
																<p>Mobile No</p>
																<p>+4463573</p>
														</li>

												</ul>
										</div>
										<div class="tollfree_block">
												<h5>toll free number</h5>
												<h3>+1-202-555-0101</h3>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>
<!--Contact Map-->
<div class="contact_map_wrapper">
		<div id="map"></div>
</div>

<?php
$this->load->view('frontend/layout/footer');
?>
