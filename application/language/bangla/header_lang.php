<?php
// title
$lang['text_title_header'] = 'টেকরাইস';
$lang['text_name_header'] = 'TechArise';

// Main Menu

$lang['website_title'] = 'হোম | বেস্ট লাইফ লিমিটেড';
$lang['home_menu'] = 'হোম';
$lang['about_menu'] = 'সম্পর্কিত';
$lang['service_menu'] = 'সেবা';
$lang['portfolio_menu'] = 'দফতর';
$lang['product_menu'] = 'আমাদের পণ্য';
$lang['contact_menu'] = 'যোগাযোগ করুন';
