<?php



// Main Menu
$lang['website_title'] = 'Home | Best Life Limited';
$lang['home_menu'] = 'Home';
$lang['about_menu'] = 'About';
$lang['service_menu'] = 'Services';
$lang['portfolio_menu'] = 'Portfolio';
$lang['product_menu'] = 'Our Products';
$lang['contact_menu'] = 'Contact Us';
