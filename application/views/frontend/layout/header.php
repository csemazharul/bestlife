<!DOCTYPE html>
<html lang="zxx">
<title>Best Life World || Home Page</title>

<head>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="kamleshyadav">
    <meta name="MobileOptimized" content="320">
    <!--Start Style -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/font.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/navigation.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/range.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/nice-select.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ui/frontend/')?>css/style.css">




    <!-- Favicon Link -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('ui/frontend/images/')?>/rsz_favicon.png">
</head>

<body>
    <div class="clv_main_wrapper index_v4">
        <div class="header3_wrapper">
            <div class="clv_header3">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        <div class="clv_left_header">
                            <div class="clv_logo">
                                <a href="index.html"><img src="<?php echo base_url('ui/frontend/images/')?>/logo11.png" alt="site Logo " style="width: 100px;" /></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10">
                        <div class="clv_right_header">
                            <div class="clv_menu">
                                <div class="clv_menu_nav">
                                    <ul>
                                        <li>
                                            <a href="<?php echo base_url() ?>">Home</a>

                                        </li>
                                        <li>
                                            <a href="javascript:;">Company</a>
                                            <ul>
                                                <li><a href="<?php echo base_url() ?>about/About">About Us</a></li>
                                                <li><a href="<?php echo base_url() ?>team/Managment">Managment</a></li>
                                            </ul>
                                        </li>
                                        <li>

                                            <a href="<?php echo base_url('allproducts') ?>">Products</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url() ?>news/News">news</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url() ?>gallery/Gallery">Gallery</a>
                                        </li>

                                        <li><a href="<?php echo base_url() ?>contact">contact us</a></li>
                                        <li><a href="<?php echo base_url() ?>user/registration">Sign Up</a></li>
                                        <li><a href="<?php echo base_url() ?>user/login">Sign In</a></li>
                                    </ul>

                                </div>
                                <div class="cart_nav">
                                    <ul>
                                        <li>
                                            <a class="search_toggle" href="javascript:;"><i class="fa fa-search" aria-hidden="true">&nbsp;Search</i></a>
                                        </li>

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
