<!--Footer-->
<div class="clv_footer_wrapper clv_section">
		<div class="container">
				<div class="row">
						<div class="col-md-3 col-lg-3">
								<div class="footer_block">
										<div class="footer_logo">
										<a href="javascript:;"><img src="<?php echo base_url('ui/frontend/images/')?>/logo11.png" alt="image" style="width:100px;" /></a>
										</div>
										<p><span>
				<svg
				 xmlns="http://www.w3.org/2000/svg"
				 xmlns:xlink="http://www.w3.org/1999/xlink"
				 width="16px" height="16px">
				<defs>
				<filter id="Filter_0">
					<feFlood flood-color="rgb(31, 161, 46)" flood-opacity="1" result="floodOut" />
					<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
					<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
				</filter>

				</defs>
				<g filter="url(#Filter_0)">
				<path fill-rule="evenodd"  fill="rgb(81, 176, 30)"
				 d="M14.873,0.856 C14.815,0.856 14.700,0.856 14.643,0.913 L0.850,6.660 C0.620,6.776 0.505,6.948 0.505,7.176 C0.505,7.465 0.677,7.695 0.965,7.752 L6.942,9.189 C7.057,9.189 7.114,9.305 7.172,9.419 L8.608,15.396 C8.666,15.626 8.896,15.855 9.183,15.855 C9.413,15.855 9.643,15.683 9.700,15.511 L15.447,1.718 C15.447,1.660 15.505,1.603 15.505,1.488 C15.447,1.085 15.217,0.856 14.873,0.856 ZM9.355,8.902 L9.068,7.695 C9.011,7.465 8.838,7.350 8.666,7.292 L7.459,7.005 C7.172,6.948 7.172,6.545 7.401,6.487 L11.022,4.993 C11.252,4.878 11.482,5.109 11.424,5.395 L9.930,9.017 C9.758,9.189 9.413,9.131 9.355,8.902 Z"/>
				</g>
				</svg></span> 512,Beand square, Califonia</p>
										<p><span><svg
				 xmlns="http://www.w3.org/2000/svg"
				 xmlns:xlink="http://www.w3.org/1999/xlink"
				 width="16px" height="15px">

				<g filter="url(#Filter_0)">
				<path fill-rule="evenodd"  fill="rgb(81, 176, 30)"
				 d="M13.866,7.235 C13.607,5.721 12.892,4.344 11.802,3.254 C10.653,2.108 9.197,1.381 7.592,1.156 L7.755,-0.002 C9.613,0.257 11.296,1.096 12.626,2.427 C13.888,3.692 14.716,5.284 15.019,7.039 L13.866,7.235 ZM10.537,4.459 C11.296,5.222 11.796,6.181 11.977,7.238 L10.824,7.436 C10.684,6.617 10.300,5.874 9.713,5.287 C9.091,4.666 8.304,4.276 7.439,4.155 L7.601,2.996 C8.719,3.151 9.734,3.657 10.537,4.459 ZM4.909,8.182 C5.709,9.162 6.611,10.033 7.689,10.711 C7.920,10.854 8.176,10.960 8.417,11.092 C8.538,11.160 8.623,11.139 8.723,11.035 C9.088,10.661 9.460,10.293 9.831,9.924 C10.318,9.440 10.931,9.440 11.421,9.924 C12.017,10.516 12.614,11.110 13.207,11.707 C13.704,12.207 13.701,12.818 13.201,13.324 C12.864,13.665 12.505,13.989 12.186,14.345 C11.721,14.866 11.140,15.035 10.472,14.997 C9.500,14.944 8.607,14.623 7.745,14.205 C5.831,13.275 4.194,11.985 2.823,10.355 C1.808,9.150 0.971,7.834 0.422,6.355 C0.153,5.639 -0.038,4.906 0.022,4.129 C0.059,3.651 0.237,3.242 0.590,2.907 C0.971,2.546 1.330,2.168 1.705,1.800 C2.192,1.319 2.804,1.319 3.295,1.797 C3.598,2.093 3.894,2.396 4.194,2.696 C4.485,2.988 4.775,3.277 5.065,3.570 C5.578,4.085 5.578,4.684 5.069,5.197 C4.703,5.565 4.341,5.933 3.969,6.293 C3.873,6.390 3.863,6.468 3.913,6.586 C4.160,7.173 4.513,7.694 4.909,8.182 Z"/>
				</g>
				</svg></span> ( +61 ) 1800-1234-1245</p>
										<p><span><svg
			 xmlns="http://www.w3.org/2000/svg"
			 xmlns:xlink="http://www.w3.org/1999/xlink"
			 width="16px" height="16px">

			<g filter="url(#Filter_0)">
			<path fill-rule="evenodd"  fill="rgb(81, 176, 30)"
			 d="M16.000,5.535 C16.000,4.982 15.680,4.507 15.280,4.191 L8.000,-0.002 L0.720,4.191 C0.320,4.507 0.000,4.982 0.000,5.535 L0.000,13.447 C0.000,14.317 0.720,15.028 1.600,15.028 L14.400,15.028 C15.280,15.028 16.000,14.317 16.000,13.447 L16.000,5.535 ZM8.000,9.491 L1.360,5.376 L8.000,1.579 L14.640,5.376 L8.000,9.491 Z"/>
			</g>
			</svg></span> bestlifeworld@yahoo.com</p>
										<ul class="agri_social_links">
												<li><a href="javascript:;"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
												<li><a href="javascript:;"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
												<li><a href="javascript:;"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a></li>
												<li><a href="javascript:;"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a></li>
										</ul>
								</div>
						</div>
						<div class="col-md-3 col-lg-3">
								<div class="footer_block">
										<div class="footer_heading">
												<h4>lorem 833</h4>
												<img src="<?php echo base_url('ui/frontend/images/')?>/garden_underline3.png" alt="image" />
										</div>
										<ul class="time_table">
												<li>
														<p><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Mon - Tue</p>
														<p>lorem945930</p>
												</li>
												<li>
														<p><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Wed - Thur</p>
														<p>lorem945930</p>
												</li>
												<li>
														<p><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Fri - Sat</p>
														<p>lorem945930</p>
												</li>
												<li>
														<p><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Sunday</p>
														<p>lorem945930</p>
												</li>
										</ul>
								</div>
						</div>
						<div class="col-md-3 col-lg-3">
								<div class="footer_block">
										<div class="footer_heading">
												<h4>latest news</h4>
												<img src="<?php echo base_url('ui/frontend/images/')?>/garden_underline3.png" alt="image" />
										</div>
										<div class="footer_post_section">
												<div class="footer_post_slide">
														<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
														<div class="blog_links">
																<p>Excepteur occaecat cupid proent.</p>
																<a href="javascript:;">https://T.Co/Sr45bvMJU6</a>
														</div>
												</div>
												<div class="footer_post_slide">
														<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
														<div class="blog_links">
																<p>Occaecat ther cupidatat proent.</p>
																<a href="javascript:;">https://T.Co/Sr45bvMJU6</a>
														</div>
												</div>
												<div class="footer_post_slide">
														<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
														<div class="blog_links">
																<p>Excepteur occaecat cupid proent.</p>
																<a href="javascript:;">https://T.Co/Sr45bvMJU6</a>
														</div>
												</div>
												<div class="footer_post_slide">
														<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
														<div class="blog_links">
																<p>Excepteur occaecat cupid proent.</p>
																<a href="javascript:;">https://T.Co/Sr45bvMJU6</a>
														</div>
												</div>
												<div class="footer_post_slide">
														<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
														<div class="blog_links">
																<p>Occaecat ther cupidatat proent.</p>
																<a href="javascript:;">https://T.Co/Sr45bvMJU6</a>
														</div>
												</div>
												<div class="footer_post_slide">
														<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
														<div class="blog_links">
																<p>Excepteur occaecat cupid proent.</p>
																<a href="javascript:;">https://T.Co/Sr45bvMJU6</a>
														</div>
												</div>
										</div>
								</div>
						</div>
						<div class="col-md-3 col-lg-3">
								<div class="footer_block">
										<div class="footer_heading">
												<h4>Map</h4>
												<img src="<?php echo base_url('ui/frontend/images/')?>/garden_underline3.png" alt="image" />
										</div>
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3746984.465708819!2d88.10026026270491!3d23.490583053663357!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30adaaed80e18ba7%3A0xf2d28e0c4e1fc6b!2sBangladesh!5e0!3m2!1sen!2sbd!4v1569495179740!5m2!1sen!2sbd" width="300" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
								</div>
						</div>
				</div>
		</div>
</div>
<!--Copyright-->
<div class="clv_copyright_wrapper">
		<p>copyright &copy; 2019 <a href="javascript:;">Best Life World.</a> all right reserved.</p>
</div>
<!--Popup-->
<div class="search_box">
		<div class="search_block">
				<h3>Explore more with us</h3>
				<div class="search_field">
						<input type="search" placeholder="Search Here" />
						<a href="javascript:;">search</a>
				</div>
		</div>
		<span class="search_close">

<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
 viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"  width="30px" height="30px">
<g>
<path style="fill:#2a7d2e;" d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
	c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
	C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
	s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/>
</g>
</svg>
</span>
</div>
<!--Payment Success Popup-->
<div class="success_wrapper">
		<div class="success_inner">
				<div class="success_img"><img src="<?php echo base_url('ui/frontend/images/')?>/success.png" alt=""></div>
				<h3>payment success</h3>
				<img src="<?php echo base_url('ui/frontend/images/')?>/clv_underline.png" alt="">
				<p>Your order has been successfully processed! Please direct any questions you have to the store owner. Thanks for shopping</p>
				<a href="javascript:;" class="clv_btn">continue browsing</a>
				<span class="success_close">

				<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
				<g>
						<path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
								c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
								l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
								c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
				</g>
				</svg>
		</span>
		</div>
</div>
<!--Thank You Popup-->
<div class="thankyou_wrapper">
		<div class="thankyou_inner">
				<div class="thankyou_img"><img src="<?php echo base_url('ui/frontend/images/')?>/thankyou.png" alt=""></div>
				<h3>your order is being processed</h3>
				<h5>We Have Just Sent You An Email With Complete Information About Your Booking</h5>
				<div class="download_button">
						<a href="javascript:;" class="clv_btn">download PDF</a>
						<a href="index.html" class="clv_btn">back to site</a>
				</div>
				<span class="success_close">

				<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
				<g>
						<path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
						c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
						l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
						c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
				</g>
				</svg>
		</span>
		</div>
</div>
<!--SignUp Popup-->
<div class="signup_wrapper">
		<div class="signup_inner">
				<div class="signup_details">
						<div class="site_logo">
								<a href="index.html"> <img src="<?php echo base_url('ui/frontend/images/')?>/logo11.png" alt="image" style="width:100px;"></a>
						</div>
						<h3>welcome to Best life WOrld !!</h3>
						<p>Consectetur adipisicing elit sed do eiusmod por incididunt uttelabore et dolore magna aliqu.</p>
						<a href="javascript:;" class="clv_btn white_btn pop_signin">sign in</a>
						<ul>
								<li><a href="javascript:;"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
								<li><a href="javascript:;"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
								<li><a href="javascript:;"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a></li>
								<li><a href="javascript:;"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a></li>
						</ul>
				</div>
				<div class="signup_form_section">
						<h4>create account</h4>
						<img src="<?php echo base_url('ui/frontend/images/')?>/clv_underline.png" alt="image">
						<div class="form_block">
								<input type="text" class="form_field" placeholder="Name">
						</div>
						<div class="form_block">
								<input type="text" class="form_field" placeholder="Email">
						</div>
						<div class="form_block">
								<input type="text" class="form_field" placeholder="Password">
						</div>
						<a href="javascript:;" class="clv_btn">sign up</a>
						<div class="social_button_section">
								<a href="javascript:;" class="fb_btn">
										<span><img src="<?php echo base_url('ui/frontend/images/')?>/fb.png" alt="image"></span>
										<span>facebook</span>
								</a>
								<a href="javascript:;" class="google_btn">
										<span><img src="<?php echo base_url('ui/frontend/images/')?>/google.png" alt="image"></span>
										<span>google+</span>
								</a>
						</div>
						<span class="success_close">

						<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
						<g>
								<path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
								c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
								l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
								c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
						</g>
						</svg>
				</span>
				</div>
		</div>
</div>
<!--SignIn Popup-->
<div class="signin_wrapper">
		<div class="signup_inner">
				<div class="signup_details">
						<div class="site_logo">
								<a href="index.html"> <img src="<?php echo base_url('ui/frontend/images/')?>/logo11.png" alt="image" style="width:100px;"></a>
						</div>
						<h3>welcome to Best Life World!</h3>
						<p>Consectetur adipisicing elit sed do eiusmod por incididunt uttelabore et dolore magna aliqu.</p>
						<a href="javascript:;" class="clv_btn white_btn pop_signup">sign up</a>
						<ul>
								<li><a href="javascript:;"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
								<li><a href="javascript:;"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
								<li><a href="javascript:;"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a></li>
								<li><a href="javascript:;"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a></li>
						</ul>
				</div>
				<div class="signup_form_section">
						<h4>sign in account</h4>
						<img src="<?php echo base_url('ui/frontend/images/')?>/clv_underline.png" alt="image">
						<div class="form_block">
								<input type="text" class="form_field" placeholder="Email">
						</div>
						<div class="form_block">
								<input type="text" class="form_field" placeholder="Password">
						</div>
						<a href="javascript:;" class="clv_btn">sign up</a>
						<div class="social_button_section">
								<a href="javascript:;" class="fb_btn">
										<span><img src="<?php echo base_url('ui/frontend/images/')?>/fb.png" alt="image"></span>
										<span>facebook</span>
								</a>
								<a href="javascript:;" class="google_btn">
										<span><img src="<?php echo base_url('ui/frontend/images/')?>/google.png" alt="image"></span>
										<span>google+</span>
								</a>
						</div>
						<span class="success_close">

						<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
						<g>
								<path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
								c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
								l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
								c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
						</g>
						</svg>
				</span>
				</div>
		</div>
</div>



<!--Main js file Style-->
<script src="<?php echo base_url('ui/frontend/')?>js/jquery.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/swiper.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/magnific-popup.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/jquery.appear.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/jquery.countTo.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/isotope.min.js"></script>

<script src="<?php echo base_url('ui/frontend/')?>js/range.js"></script>

<script src="<?php echo base_url('ui/frontend/')?>js/revolution.extension.actions.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/revolution.extension.kenburn.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/revolution.extension.migration.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/revolution.extension.parallax.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/revolution.extension.video.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/custom.js"></script>
<script src="<?php echo base_url('ui/frontend/')?>js/custom_jquery.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    jQuery(
        function($) {

            $('#message').fadeIn (5050);

            $('#message').fadeOut (5050);
        }

    )

</script>
</body>

</html>
