<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<title>Best Life Ltd</title>
	<!--favicon-->
<link rel="shortcut icon" type="image/png" href="<?php echo base_url('ui/frontend/images/')?>/rsz_favicon.png">
	<!-- Bootstrap core CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/bootstrap.min.css" rel="stylesheet"/>
	<!-- animate CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/animate.css" rel="stylesheet" type="text/css"/>
	<!-- Icons CSS-->
	<link href="<?php echo base_url(); ?>ui/backend/css/icons.css" rel="stylesheet" type="text/css"/>
	<!-- Custom Style-->
	<link href="<?php echo base_url(); ?>ui/backend/css/app-style.css" rel="stylesheet"/>

</head>

<body class="authentication-bg">
