
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->


<!--Start footer-->
<footer class="footer">
	<div class="container">
		<div class="text-center">
			Copyright © 2019 Best Life World. All Right Reserved.
		</div>
	</div>
</footer>
<!--End footer-->
<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url(); ?>ui/backend/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>ui/backend/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>ui/backend/js/bootstrap.min.js"></script>

<!-- simplebar js -->
<script src="<?php echo base_url(); ?>ui/backend/plugins/simplebar/js/simplebar.js"></script>
<!-- waves effect js -->
<script src="<?php echo base_url(); ?>ui/backend/js/waves.js"></script>
<!-- sidebar-menu js -->
<script src="<?php echo base_url(); ?>ui/backend/js/sidebar-menu.js"></script>
<!-- Custom scripts -->
<script src="<?php echo base_url(); ?>ui/backend/js/app-script.js"></script>

<!-- Vector map JavaScript -->
<script src="<?php echo base_url(); ?>ui/backend/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?php echo base_url(); ?>ui/backend/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- Sparkline JS -->
<script src="<?php echo base_url(); ?>ui/backend/plugins/sparkline-charts/jquery.sparkline.min.js"></script>
<!-- Chart js -->
<script src="<?php echo base_url(); ?>ui/backend/plugins/Chart.js/Chart.min.js"></script>
<!--notification js -->
<!--<script src="--><?php //echo base_url(); ?><!--ui/backend/plugins/notifications/js/lobibox.min.js"></script>-->
<!--<script src="--><?php //echo base_url(); ?><!--ui/backend/plugins/notifications/js/notifications.min.js"></script>-->
<!-- Index js -->
<script src="<?php echo base_url(); ?>ui/backend/js/index.js"></script>
<script src="<?php echo base_url(); ?>ui/backend/js/custom.js"></script>

</div><!--End wrapper-->


</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Feb 2019 03:56:12 GMT -->
</html>
