<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controllers functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controllers class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controllers class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controllers/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controllers and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controllers and method URI segments.
|
| Examples:	my-controllers/index	-> my_controller/index
|		my-controllers/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'page/page/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['about/(:any)']='page/page/about';
$route['contact']='page/page/contact';
$route['team/(:any)']='page/page/team';
$route['news/(:any)']='page/page/news';
$route['single_news/(:any)']='page/page/singleNews';
$route['vission/(:any)']='page/page/vission';
$route['mission/(:any)']='page/page/mission';

$route['allproducts']='page/page/products';
$route['gallery/(:any)']='page/page/gallery';
$route['admin']='page/page/admin';
// admin authentication
$route['register']='admin_login/user/register';
$route['login']='admin_login/user/loginForm';
$route['register/store']='admin_login/user/register_user';
$route['login/store']='admin_login/user/login_user';
$route['verified/(:any)']='admin_login/user/verified';
// user authentication
$route['user/registration']='user_login/user/register';
$route['user/login']='user_login/user/loginForm';
$route['user/login/store']='user_login/user/login_user';
$route['user/registration/store']='user_login/user/register_user';
$route['user/verified/(:any)']='user_login/user/verified';
$route['user/dashboard']='user_login/user/userDashboard';
$route['user/list']='user_login/user/userList';
$route['add-fund']='user_login/user/addFund';
$route['fund/store']='user_login/user/fundStore';
$route['fund/list']='fund/fund/fundList';
$route['add_purchase/(:any)']='fund/fund/addPurchase';
$route['purchase/store']='fund/fund/purchaseStore';
$route['fund/status/(:any)/(:any)/(:any)']='fund/fund/fundStatus';
$route['products']='fund/fund/allProducts';
$route['fund/transfer']='fund/fund/addTransferFund';
$route['withdraw/add']='fund/fund/withdrwaAdd';
$route['withdraw/store']='fund/fund/withdrawStore';
$route['transferfund/store']='fund/fund/transferStore';
$route['withdraw/history']='fund/fund/withdrawHistory';
$route['transfer/history']='fund/fund/transferHistory';
$route['withdraw/status/(:any)/(:any)/(:any)']='fund/fund/withdrawStatus';
$route['withdraw']='fund/fund/singleWithdraw';
$route['account/info']='user_login/user/accountInfo';
$route['view_tpin']='user_login/user/viewTpin';
$route['tpin_show']='user_login/user/tpinShow';
$route['purchase/(:any)']='fund/fund/productPurchase';
$route['change_tpin']='user_login/user/changeTpin';
$route['change_tpin/update']='user_login/user/tpinUpdate';
$route['change_pasword']='user_login/user/chnagePassword';
$route['password/update']='user_login/user/passwordUpdate';

$route['page/create']='page/page/pageCreate';
$route['page/store']='page/page/pageStore';
$route['page/list']='page/page/pageList';
$route['page/edit/(:any)']='page/page/pageEdit';
$route['pagination']='page/page/pagination';//case sensetive bai eto gula korci  sob gula dek kivabe korci... deksos route e ki prblm
$route['page/update/(:any)']='page/page/pageUpdate';
$route['page/delete/(:any)']='page/page/delete';
$route['sub_product/(:any)']='page/page/subProduct';
$route['slider/create']='slider/slider/sliderCreate';
$route['slider/store']='slider/slider/sliderStore';
$route['slider/list']='slider/slider/sliderList';
$route['slider/edit/(:any)']='slider/slider/sliderEdit';
$route['slider/update/(:any)']='slider/slider/sliderUpdate';
$route['slider/delete/(:any)']='slider/slider/delete';
//prameter pass kor
$route['category/create']='category/category/categoryCreate';
$route['category/store']='category/category/categoryStore';
$route['category/list']='category/category/categoryList';
$route['category/edit/(:any)']='category/category/categoryEdit';
$route['category/update/(:any)']='category/category/categoryUpdate';
$route['category/delete/(:any)']='category/category/delete';
$route['product/create']='product/product/productCreate';
$route['product/store']='product/product/productStore';
$route['product/list']='product/product/productList';


$route['product/edit/(:any)']='product/product/productEdit';
$route['product/update/(:any)']='product/product/productUpdate';

$route['gallery/create']='gallery/gallery/galleryCreate';
$route['gallery/store']='gallery/gallery/galleryStore';
$route['gallery/list']='gallery/gallery/galleryList';
$route['gallery/edit/(:any)']='gallery/gallery/getGallery';
$route['gallery/update/(:any)']='gallery/gallery/galleryUpdate';
$route['gallery/delete/(:any)']='gallery/gallery/delete';


$route['sub/create']='subcategory/subcategory/subCreate';
$route['sub/store']='subcategory/subcategory/subStore';
$route['sub/edit/(:any)']='subcategory/subcategory/subCategoryEdit';
$route['sub/update/(:any)']='subcategory/subcategory/subUpdate';
$route['sub/list']='subcategory/subcategory/subCategoryList';
$route['findsubcategory/(:any)']='subcategory/subcategory/findSubcategory';
$route['sub/delete/(:any)']='subcategory/subcategory/delete';
