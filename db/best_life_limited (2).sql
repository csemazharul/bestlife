-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 12, 2019 at 06:02 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `best_life_limited`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('1','0') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1:Active, 0:Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `status`) VALUES
(3, 'Organic Farming', '2019-10-11 06:21:00', '2019-10-11 06:21:00', '1'),
(4, 'Organic Green Bell Pepper', '2019-10-11 06:22:00', '2019-10-11 06:22:00', '1'),
(5, 'Permaculture', '2019-10-11 06:22:00', '2019-10-11 06:22:00', '1'),
(6, 'Precision Farming', '2019-10-11 06:22:00', '2019-10-11 06:22:00', '1'),
(7, 'Conservation Agriculture', '2019-10-11 06:23:00', '2019-10-11 06:23:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `fund_add_req`
--

CREATE TABLE `fund_add_req` (
  `id` int(100) NOT NULL,
  `user_id` int(150) NOT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `t_pin` varchar(250) DEFAULT NULL,
  `fund_trn_st` int(150) DEFAULT '0' COMMENT '0=Not Approved,1=Approved',
  `date_time` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `request_name` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fund_add_req`
--

INSERT INTO `fund_add_req` (`id`, `user_id`, `amount`, `t_pin`, `fund_trn_st`, `date_time`, `date`, `approved_at`, `request_name`) VALUES
(7, 81, 10.00, '123', 1, '2019-10-09 08:04:00', '2019-10-09', NULL, 'Mazharul '),
(8, 81, 874.00, '874989', 1, '2019-10-09 05:28:00', '2019-10-09', NULL, 'Mazharul '),
(9, 81, 22.00, '874989', 1, '2019-10-09 05:30:00', '2019-10-09', NULL, 'Mazharul '),
(10, 82, 874.00, '874989', 1, '2019-10-09 06:44:00', '2019-10-09', NULL, 'Mazharul ');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(11) NOT NULL,
  `title` varchar(111) NOT NULL,
  `short_description` varchar(111) NOT NULL,
  `picture` varchar(111) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `title`, `short_description`, `picture`, `created_at`, `updated_at`) VALUES
(1, '1212', '11', 'd_p_14.jpg', '2019-10-06 03:51:00', '2019-10-06 04:12:00');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_code` int(11) NOT NULL,
  `mobile_no` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'just like facebook user name',
  `reg_username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'created by user in registration form',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `security_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tmp_password` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_activation_link` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT '3' COMMENT '1=supoer_admin,2=agent_admin,3=user,4=subadmin',
  `verify_status` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=inactive,1=active,2=delete,3=pending for email activation',
  `member_type` int(11) NOT NULL DEFAULT '0' COMMENT 'for promotion or offer',
  `sponser_id` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'login id of that user',
  `refferal_tree` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `c_balance` double(10,2) DEFAULT '0.00',
  `refferal` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_act_st` int(11) DEFAULT '1' COMMENT 'user act status 1= not upgrade ,2=Upgrade',
  `payment_method` varchar(250) COLLATE utf8_unicode_ci DEFAULT '0',
  `pay_acc_no` varchar(250) COLLATE utf8_unicode_ci DEFAULT '0',
  `rank_type` int(150) DEFAULT '0',
  `profile_pic` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_balance` decimal(10,2) DEFAULT '0.00',
  `s_balance` double(10,2) DEFAULT '0.00',
  `upgrade_st` int(11) DEFAULT '0' COMMENT '0=Not Upgrade,1=Upgrade',
  `package` int(1) DEFAULT '1',
  `pkg_id` int(1) DEFAULT NULL,
  `cover_pic` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `currency` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `t_pin` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `tre_pin` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `doj` date DEFAULT NULL,
  `id_no` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intro_id` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intro_pos` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `placement_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `email`, `country_code`, `mobile_no`, `username`, `reg_username`, `password`, `security_code`, `tmp_password`, `account_activation_link`, `user_type`, `verify_status`, `member_type`, `sponser_id`, `refferal_tree`, `created_at`, `updated_at`, `c_balance`, `refferal`, `user_act_st`, `payment_method`, `pay_acc_no`, `rank_type`, `profile_pic`, `f_balance`, `s_balance`, `upgrade_st`, `package`, `pkg_id`, `cover_pic`, `firstname`, `lastname`, `fullname`, `date_of_birth`, `currency`, `user_id`, `t_pin`, `tre_pin`, `doj`, `id_no`, `intro_id`, `intro_pos`, `placement_id`, `dob`) VALUES
(81, 'mdmazharulislam433@gmail.com', 0, '0121232', 'Arif', NULL, '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL, 3, '1', 0, '123', '', '2019-10-07 05:25:00', '2019-10-10 05:03:41', 100.00, NULL, 1, '0', '0', 0, NULL, '0.00', 0.00, 0, 1, NULL, NULL, 'arif', 'Islam', 'Mazharul Islam', '2019-10-07', 'EURO', 'sdfsadf', '874989', '212', NULL, NULL, NULL, NULL, NULL, ''),
(82, 'admin@gmail.com', 0, 'sdfsdf', 'Sahed', NULL, '81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, NULL, 3, '1', 0, '123', '', '2019-10-07 05:27:00', '2019-10-10 10:14:17', 100.00, NULL, 1, '0', '0', 0, NULL, '0.00', 0.00, 0, 1, NULL, NULL, 'sahed', 'safsadf', 'Mazharul Islam', '2019-10-08', 'USD', '4234234234', '127', '121212', NULL, NULL, NULL, NULL, NULL, ''),
(83, 'mazharulislam10000@gmail.com', 0, '1212', 'Sukkur', NULL, '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL, 3, '1', 0, '123', '', '2019-10-09 08:33:00', '2019-10-12 10:15:00', 20.00, NULL, 1, '0', '0', 0, NULL, '0.00', 0.00, 0, 1, NULL, NULL, 'Sukkur', 'islam', 'Mazharul Islam', '2019-10-14', 'USD', '123', '124', '123', NULL, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(250) NOT NULL,
  `title` varchar(191) NOT NULL,
  `short_description` text NOT NULL,
  `long_description` text NOT NULL,
  `photo` varchar(191) DEFAULT NULL,
  `created_by` varchar(111) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `menu_name`, `title`, `short_description`, `long_description`, `photo`, `created_by`, `created_at`, `updated_at`) VALUES
(20, 'About', 'Welcome To Best Life ', 'Lorem Ipsum Is Simply Dummy Text Of The Printing', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitatiomco laboris nisi ut aliquip ex ea commodo consequat.\r\n\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fuiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui offi deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error eivoluptatem accusantium doloremque laudantium.', '2121.png', 'mazharul islam', '2019-10-10 23:20:00', '2019-10-10 23:20:00'),
(21, 'News', 'Finding reveal how neem cells', 'Duis aute irure dolor in reprehenderit itate velit esse cillum dolore eu fugiat nullriaturr sineropit occaecat cupidataon.', 'Duis aute irure dolor in reprehenderit itate velit esse cillum dolore eu fugiat nullriaturr sineropit occaecat cupidataon.', 'blog3.jpg', 'mazharul islam', '2019-10-09 21:11:00', '2019-10-09 21:11:00'),
(26, 'About', 'Welcome To Bestlife World', 'Lorem Ipsum Is Simply Dummy Text Of The Printing', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitatiomco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit', '21.png', 'mazharul islam', '2019-10-09 21:39:00', '2019-10-09 21:39:00'),
(27, 'Managment', 'Rouis R. Weons', 'Poultry Farmers', 'Shamin@gmail.com', 'dairy_team4.jpg', 'mazharul islam', '2019-10-09 22:48:00', '2019-10-09 22:48:00'),
(28, 'Managment', 'Rouis R. Weons', 'Poultry Farmers', 'nuru@gmail.com', 'dairy_team3.jpg', 'mazharul islam', '2019-10-09 23:10:00', '2019-10-09 23:10:00'),
(29, 'Managment', 'Rouis R. Weons', 'Poultry Farmers', 'rouson@gmail.com\r\n', 'coffee_team3.jpg', 'mazharul islam', '2019-10-09 23:13:00', '2019-10-09 23:13:00'),
(30, 'Managment', 'Rouis R. Weons', 'Poultry Farmers', 'sukkur@gmail.com', 'dairy_team1.jpg', 'mazharul islam', '2019-10-09 23:15:00', '2019-10-09 23:15:00'),
(31, 'Gallery', '', '', '', 'gallery4_big.jpg', 'mazharul islam', '2019-10-09 23:24:00', '2019-10-09 23:24:00'),
(32, 'Gallery', '', '', '', 'dairy_gallery_big51.jpg', 'mazharul islam', '2019-10-09 23:25:00', '2019-10-09 23:25:00'),
(33, 'Gallery', '', '', '', 'gallery3.jpg', 'mazharul islam', '2019-10-09 23:26:00', '2019-10-09 23:26:00'),
(34, 'Our Vision', 'Our Vision', 'Dolor sit amet, consectetur adipisicing elit impor incididunt ut labore et dolore magna.', 'Dolor sit amet, consectetur adipisicing elit impor incididunt ut labore et dolore magna.', 'vision.png', 'mazharul islam', '2019-10-10 00:24:00', '2019-10-10 00:24:00'),
(35, 'Our Mission', 'Our Mission', 'Dolor sit amet, consectetur adipisicing elit impor incididunt ut labore et dolore magna.', 'Dolor sit amet, consectetur adipisicing elit impor incididunt ut labore et dolore magna.', 'mission.png', 'mazharul islam', '2019-10-10 00:25:00', '2019-10-10 00:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_id` int(11) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `short_description` varchar(191) NOT NULL,
  `picture` varchar(191) NOT NULL,
  `price` int(11) NOT NULL,
  `point` double DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `sub_id`, `title`, `short_description`, `picture`, `price`, `point`, `created_at`, `updated_at`) VALUES
(3, 0, NULL, 'badacopi', 'badacopi', '404_veg1.png', 30, 10, '2019-10-07 04:53:00', '2019-10-10 22:55:00'),
(4, 0, NULL, 'potato', 'healthy food', 'org_product21.jpg', 22, 30, '2019-10-10 00:57:00', '2019-10-10 22:55:00'),
(5, 0, NULL, 'vutta', 'helthy food', '404_veg11.png', 60, 50, '2019-10-10 00:58:00', '2019-10-10 22:56:00');

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `request_name` varchar(111) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(111) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `point` float DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `title` varchar(191) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `short_description` varchar(191) NOT NULL,
  `picture` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `page_id`, `short_description`, `picture`, `created_at`, `updated_at`) VALUES
(6, 'best life World', 0, 'best life World							', 'index2_slider_bg2.jpg', '2019-10-03 01:04:00', '2019-10-03 01:04:00'),
(8, 'best life World', 0, '', 'index4_slider_bg2.jpg', '2019-10-03 01:38:00', '2019-10-03 01:38:00');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(11) NOT NULL,
  `title` varchar(111) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `title`, `category_id`, `created_at`, `updated_at`) VALUES
(3, 'subcategory 1', 3, '2019-10-11 01:15:00', '2019-10-11 23:21:00'),
(4, 'subcategory 2', 5, '2019-10-11 01:15:00', '2019-10-11 23:21:00'),
(5, 'subcategory 3', 5, '2019-10-11 22:41:00', '2019-10-11 23:21:00'),
(6, 'subcategory 4', 6, '2019-10-11 22:41:00', '2019-10-11 23:21:00'),
(7, 'subcategory 6', 4, '2019-10-11 23:22:00', '2019-10-11 23:22:00'),
(8, 'subcategory 6', 3, '2019-10-11 23:24:00', '2019-10-11 23:24:00'),
(9, 'subcategory 9', 3, '2019-10-11 23:25:00', '2019-10-11 23:25:00'),
(10, 'subcategory 9', 7, '2019-10-11 23:39:00', '2019-10-11 23:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `transfer`
--

CREATE TABLE `transfer` (
  `trans_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `tr_date` datetime DEFAULT NULL,
  `deposit_type` int(11) DEFAULT NULL,
  `sender_name` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transfer`
--

INSERT INTO `transfer` (`trans_id`, `sender_id`, `amount`, `receiver_id`, `tr_date`, `deposit_type`, `sender_name`) VALUES
(22, 81, '5.00', 82, '2019-10-09 00:00:00', NULL, 'Mazharul '),
(23, 81, '10.00', 83, '2019-10-09 00:00:00', NULL, 'Mazharul '),
(24, 82, '74.00', 83, '2019-10-09 00:00:00', NULL, 'Mazharul '),
(25, 82, '100.00', 83, '2019-10-09 00:00:00', NULL, 'Mazharul ');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `email_verified` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `user_email`, `user_password`, `email_verified`) VALUES
(34, 'mazharul islam', 'mazharulislam10000@gmail.com', '202cb962ac59075b964b07152d234b70', '1'),
(35, 'mazharul islam', 'csemazharulislam@gmail.com', '202cb962ac59075b964b07152d234b70', '1');

-- --------------------------------------------------------

--
-- Table structure for table `withdraw_fund`
--

CREATE TABLE `withdraw_fund` (
  `withdraw_id` int(150) NOT NULL,
  `withdraw_user_id` int(150) DEFAULT NULL,
  `request_name` varchar(111) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `fund_trn_st` int(150) DEFAULT NULL COMMENT '0= Wait For approval,1= Approved',
  `created_at` datetime DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `withdraw_by` int(150) DEFAULT NULL,
  `wallet_type` int(150) DEFAULT NULL,
  `amount_withdraw` decimal(10,2) DEFAULT NULL,
  `with_wallet_title` varchar(250) DEFAULT NULL,
  `s_wallet` decimal(10,2) DEFAULT NULL,
  `admin_charge` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `withdraw_fund`
--

INSERT INTO `withdraw_fund` (`withdraw_id`, `withdraw_user_id`, `request_name`, `amount`, `fund_trn_st`, `created_at`, `approved_at`, `withdraw_by`, `wallet_type`, `amount_withdraw`, `with_wallet_title`, `s_wallet`, `admin_charge`) VALUES
(6, 83, 'Sukkur', '50.00', 1, '2019-10-10 00:00:00', '2019-10-10 00:00:00', NULL, NULL, '47.50', NULL, NULL, '2.50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fund_add_req`
--
ALTER TABLE `fund_add_req`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer`
--
ALTER TABLE `transfer`
  ADD PRIMARY KEY (`trans_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdraw_fund`
--
ALTER TABLE `withdraw_fund`
  ADD PRIMARY KEY (`withdraw_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fund_add_req`
--
ALTER TABLE `fund_add_req`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `transfer`
--
ALTER TABLE `transfer`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `withdraw_fund`
--
ALTER TABLE `withdraw_fund`
  MODIFY `withdraw_id` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
